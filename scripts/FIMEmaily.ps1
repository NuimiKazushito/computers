﻿
Import-Module ActiveDirectory

Get-ADGroupMember -Identity "fimstudent" -Recursive | 
Get-ADUser -Properties Mail | 
Select-Object Mail | Export-CSV -Path w:\Weby\esport\scripts\fimstudent2.txt -NoTypeInformation

(Get-Content w:\Weby\esport\scripts\fimstudent2.txt) -replace '"', '' | Set-Content w:\Weby\esport\scripts\fimstudent2.txt


#nacteni vsech uzivatelu ve skupine fim student
dsget group "CN=fimstudent,OU=Groups.std,OU=Adm,DC=uhk,DC=cz" -members > w:\Weby\esport\scripts\fimstudent.txt
#odstraneni zbytecnych informaci o studentech FIM
(Get-Content w:\Weby\esport\scripts\fimstudent.txt) -replace ',OU=student,OU=fim,DC=uhk,DC=cz"', '@uhk.cz' | Set-Content w:\Weby\esport\scripts\fimstudent.txt
#odstraneni prebytecnych informaci
(Get-Content w:\Weby\esport\scripts\fimstudent.txt) -replace '"CN=', '' | Set-Content w:\Weby\esport\scripts\fimstudent.txt
#filtrace nezadoucich uctu z ostatnich fakult a jinych testovacich uctu
Get-Content w:\Weby\esport\scripts\fimstudent.txt | Where { $_ -notmatch "DC=uhk,DC=cz" -and  $_ -notmatch "cloud" -and  $_ -notmatch "test" } | Set-Content w:\Weby\esport\scripts\filteredfile.txt
#serazeni podle abecedy
gc w:\Weby\esport\scripts\filteredfile.txt | sort | get-unique > w:\Weby\esport\scripts\fimstudent.txt
#mazani pomocnych souboru
del w:\Weby\esport\scripts\filteredfile.txt


Get-Content w:\Weby\esport\scripts\fimstudent2.txt, w:\Weby\esport\scripts\fimstudent.txt | Set-Content w:\Weby\esport\scripts\FIMemaily.txt
del w:\Weby\esport\scripts\fimstudent.txt
del w:\Weby\esport\scripts\fimstudent2.txt


  