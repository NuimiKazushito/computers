$(document).on('click', '[data-edit-attendance]', function()
    {
        $.get('/ajax/getUserAttendance/' + $(this).data('editAttendance'),{},function(data)
            {
                data = JSON.parse(data);
                $('#program').val(data['program']);
                var d = new Date(data['date']['date']);
                $('#date').val(d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + d.getDate());
                $('#workFrom').val(data['workFrom']['h'] + ':' + ('0' + data['workFrom']['i']).slice(-2));
                $('#workTo').val(data['workTo']['h'] + ':' + ('0' + data['workTo']['i']).slice(-2));
                $('#workTo').attr('disabled', false);
                $('#workHours').val(data['workHours']);
                $('#description').val(data['description']);
                $('#submitAttendance').text('Upravit docházku');
                $('#attendanceID').val(data['id']);
                $('#modal-editAttendance').modal('show');
            }
        );
    }
);