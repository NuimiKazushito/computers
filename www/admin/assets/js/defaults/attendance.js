$(document).on('click', '[data-check-state]', function()
    {
        $.get('/ajax/changeAttendanceState/' + $(this).data('checkState'), {}, function (data)
            {
                $('#allAttendances').html(data);
            }
        );
    }
);

$(document).on('click', '[data-delete-attendance-admin]', function()
    {
        $.get('/ajax/deleteAttendance/' + $(this).data('deleteAttendanceAdmin'), {admin:1}, function (data)
            {
                $('#allAttendances').html(data);
            }
        );
    }
);


$(document).on('click', '[data-generate-to-excel]', function()
    {
        window.location.href= '/attendance/generateReport?program='+$('#program').val()+'&month='+$('#month').val()+'&user='+$('#user').val()+'&contract='+$('#contract').val()+'&year='+$('#year').val();
    }
);

function renderNewData(type)
{
    var ajax = '/ajax/setExcelFilters'
    if (type === 2)
    {
        ajax = '/ajax/setWorkExcelFilters'
    }

    $.get(ajax,
        {
            program: $('#program').val(),
            month: $('#month').val(),
            user: $('#user').val(),
            contract: $('#contract').val()? $('#contract').val() : null,
            year: $('#year').val()
        }, function(data)
        {
            $('#allAttendanceFiltered').html(data);
        }
    );
}


$(document).on('click', '[data-change-report-view]', function()
    {
        var change = $('#' + $(this).data('changeReportView'));
        var changeActive = $('#' + $(this).data('changeReportViewActive'));
        change.removeClass('active');
        changeActive.addClass('active');
        change.removeClass('disabledEvent');
        changeActive.addClass('disabledEvent');

        $.get('/ajax/changeReportView',
            {
                program: $('#program').val(),
                month: $('#month').val(),
                user: $('#user').val(),
                year: $('#year').val()
            }, function (data)
            {
                $('#allAttendanceFiltered').html(data);
            }
        );
    }
);