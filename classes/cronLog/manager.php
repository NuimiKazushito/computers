<?php

namespace Classes\cronLog;

use Classes\DateTimeUtil;
use Classes\Dibi;
use Dibi\Exception;

class Manager extends Dibi
{
    const TABLE = 'tCronLog';
    const PRIMARY = 'id';

    const DATE = 'createDate';
    const FUN = 'function';
    const STATEMENT = 'statement';

    public function getDistinct(bool $onlyQuery = false): \Dibi\Fluent|array
    {
        $query = $this->db->select('DISTINCT %n', self::DATE)
            ->select(self::FUN)
            ->select(self::STATEMENT);
        $query->from(self::TABLE);
        $query->orderBy(self::DATE)->desc();

        if($onlyQuery)
        {
            return $query;
        } else {
            return $query->fetchAll();
        }
    }

    public function deleteOld(): bool
    {
        $now = new DateTimeUtil();
        $now->modify('-1 month');
        try {
            $this->db->delete(self::TABLE)
                ->where('%n <= %dt', self::DATE, $now)
                ->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}