<?php

namespace Classes\CronLog;

use Classes\DateTimeUtil;
use ReflectionMethod;

class CronLogger
{
    private Manager $manager;

    private string $functionName;
    private array $processed;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;

        $trace = debug_backtrace();
        $this->functionName = str_replace('render', '', $trace[$trace[1]['class'] == 'Cron' ? 1 : 0]['function']);
        $this->processed = [];
    }

    public function begin(): void
    {
        $this->insert('Begin');
    }

    public function end(): void
    {
        $this->manager->deleteOld();
        $this->insert(sprintf('There was %d processed. (%s)',
            count($this->processed), implode(', ', $this->processed)));
//        $this->insert('End');
    }

    public function add(int $value): void
    {
        $this->processed[] = $value;
    }
    private function insert(string $statement): void
    {
       $this->manager->managerInsert([
                Manager::DATE => new DateTimeUtil(),
                Manager::FUN => $this->functionName,
                Manager::STATEMENT => $statement
            ], true
        );
    }

}