<?php

namespace Classes\cronLog;

use Classes\StaticFunctions;
use Classes\Super\Paginator;

class Facade
{
    private Paginator $paginator;
    private Manager $manager;

    public function __construct(Paginator $paginator, Manager $manager)
    {
        $this->paginator = $paginator;
        $this->manager = $manager;
    }
    public function getData(int $page, bool $html = false): array|string
    {
        $query = $this->manager->getDistinct(true);
        $this->paginator->setQueryLimit($query, $this->paginator->getOffset($page));

        $data = $query->fetchAll();
        if ($html)
        {
            return StaticFunctions::renderLatte('cronTable', [
                    'cronLog' => $data,
                    'pages' =>  $this->paginator->getNumberOfPages(),
                    'page' =>  $page,
                    'ajax' => 'cronLoggerTable'
                ]
            );
        } else {
            return [
                'pages' =>  $this->paginator->getNumberOfPages(),
                'data' => $data
            ];
        }
    }

}