<?php

namespace Classes\Super;

use Classes\UHKEmails\Manager as UHKManager;

class Converter
{
    private string $txtFile;
    private UHKManager $UHKEmails;
    public function __construct(UHKManager $UHKEmails)
    {
        $this->txtFile = file_get_contents('./scripts/FIMemaily.txt');
        $this->UHKEmails = $UHKEmails;
    }

    public function parseTextDocument(string $email): bool
    {
        return in_array($email, array_map('strtolower', preg_split('/\s+/', $this->txtFile)));
    }

    public function insertToDatabase()
    {
        $parsedFile = preg_split('/\s+/', $this->txtFile);
        unset($parsedFile[0]);
        foreach ($parsedFile as $email)
        {
            $shortenedEmail = $login = null;
            if (substr_count($email, '.') > 1)
            {
                $parsedEmail = explode('.', $email);
                $firstName = $parsedEmail[0];
                $number = 1;
                if (count($parsedEmail) === 3)
                {
                    $parseRemaining = explode('@', $parsedEmail[1]);
                    $secondName = $parseRemaining[0];
                } else {

                    $secondName = $parsedEmail[1];
                    $parseRemaining = explode('@', $parsedEmail[1]);
                    $number = $parseRemaining[0];
                }
                $login = substr($secondName, 0, 5) . substr($firstName, 0, 2) . $number;
                $shortenedEmail = $login.'@uhk.cz';
            }

            if (!is_null($shortenedEmail) && array_search($login.'@uhk.cz', $parsedFile))
            {
                unset($parsedFile[array_search($login.'@uhk.cz', $parsedFile)]);
            }

            $this->UHKEmails->managerInsert([
                    UHKManager::LOGIN => $login,
                    UHKManager::NORMAL_EMAIL => $email,
                    UHKManager::SHORTENED_EMAIL => $shortenedEmail
                ]
            );
        }
    }

    public function test()
    {
        dumpe(preg_split('/\s+/', $this->txtFile));
    }
}