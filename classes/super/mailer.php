<?php
namespace Classes\Super;

use Classes\DateTimeUtil;
use Classes\StaticFunctions;
use Dibi\DateTime;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class Mailer
{
    const LOCALHOST = '127.0.0.1';
    private $server;
    private PHPMailer $mail;

    public function __construct()
    {
        $this->mail = $this->getMail();
        $this->server = array_key_exists('SERVER_ADDR', $_SERVER) ? $_SERVER['SERVER_ADDR'] : '';
    }

    private function getMail(bool $debug = false): PHPMailer
    {
        $mail = new PHPMailer($debug);
        if ($debug)
        {
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        }
        $mail->isSMTP();
        $mail->Host       = 'posta.uhk.cz';
        $mail->SMTPAuth   = true;
        $mail->Username   = 'noreply.imit@uhk.cz';
        $mail->Password   = '22MbxProWebSrv.12';                        //SMTP password
        //$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port       = 465;
        $mail->CharSet = 'UTF-8';

        $mail->setFrom('noreply.imit@uhk.cz', 'Ucebna Esport FIM');

        return $mail;
    }

    private function sendMail(): bool
    {
        if($this->server != self::LOCALHOST)
        {
            try {
                $this->mail->send();
                return true;
            } catch (Exception $e) {
                bdump($e);
                return false;
            }
        }
        return false;
    }

    private function getBody(string $template, array $data): string
    {
        return StaticFunctions::renderLatte($template, $data, 'www/email');
    }

    public function sendRegistrationMessage(string $email, string $key, int|string $pc, string $user,
                                            DateTimeUtil $timeFrom, DateTimeUtil $timeTo, int $reservationID = 0) :bool
    {
        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Potvrzení registrace do učebny esportu FIM';
        $this->mail->Body    = $this->getBody('registration', [
                'id' => $reservationID,
                'pc' => $pc,
                'user' => $user,
                'key' => $key,
                'timeFrom' => $timeFrom->getEmailDateTimeFormat(),
                'timeTo' => $timeTo->getEmailDateTimeFormat(),
            ]
        );

        return $this->sendMail();
    }

    public function sendDennyReservationFinal(string $email, DateTime $from, DateTime $to): bool
    {
        $from = new DateTimeUtil($from);
        $to = new DateTimeUtil($to);

        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o zamítnutí rezervace celé učebny esportu FIM';
        $this->mail->Body = $this->getBody('dennyReservationFinal', [
                'whenFrom' => $from->getEmailDateTimeFormat(),
                'whenTo' => $to->getEmailDateTimeFormat()
            ]
        );
        return $this->sendMail();
    }

    private function sendTestEmail(): void
    {
        $mail = $this->getMail(true);
        $mail->addAddress('vondrda3@uhk.cz');

        $mail->isHTML(true);
        $mail->Subject = 'Test';
        $mail->Body    = '<p>Dobrý den</p>,
                          <p>Jedná se o testovací zprávu</p>.
                          <p>S pozdravem a přáním hezkého dne</p>,
                          <p>Váš podpůrný tým esportu</p>';

        if($this->server != self::LOCALHOST)
        {
            try {
                $mail->send();
            } catch (Exception $e) {
                bdump($e);
            }
        }
    }

    public function sendReservationAskMail(array $data): bool
    {
//            $mail->addAddress('jan.ferkl@uhk.cz');
        $this->mail->addAddress('vondrda3@uhk.cz');

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o vytvoření rezervace do učebny esportu FIM';
        $this->mail->Body    = sprintf('<p>Dobrý den</p>,
                          <p>do esport učebny byl vytvořen požadavek na rezervaci celé učebny.</p>
                          <p>Požadavek byl zadán od: %s (%s)</p>
                          <p>Více informací k této rezervaci lze zjistit v <a target="_blank" href="https://imitweby.uhk.cz/esport/admin/reservationAsk">esport administraci</a></p>. 
                          <p>S pozdravem a přáním hezkého dne,</p>
                          <p>Váš podpůrný tým esportu</p>',
            $data['name'], $data['email']
        );
        return $this->sendMail();
    }

    public function sendAcceptReservation($email, DateTimeUtil $whenFrom, DateTimeUtil $whenTo): bool
    {
        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o potvrzení rezervace učebny esportu FIM';
        $this->mail->Body    = $this->getBody('acceptReservation', [
                'whenFrom' => $whenFrom->getEmailDateTimeFormat(),
                'whenTo' => $whenTo->getEmailDateTimeFormat()
            ]
        );
        return $this->sendMail();
    }

    public function sendRefuseReservation($email, DateTimeUtil $whenFrom, DateTimeUtil $whenTo, string $reason): bool
    {
        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o zamítnutí rezervace do učebny esportu FIM';
        $this->mail->Body    = $this->getBody('refuseReservation', [
                'whenFrom' => $whenFrom->getEmailDateTimeFormat(),
                'whenTo' => $whenTo->getEmailDateTimeFormat(),
                'reason' => $reason
            ]
        );
        return $this->sendMail();
    }

    public function sendAcceptReservationFinal(string $email, DateTime $from, DateTime $to, string $hashKey): bool
    {
        $whenFrom = new DateTimeUtil($from);
        $whenTo = new DateTimeUtil($to);

        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o potvrzení rezervace učebny esportu FIM';
        $this->mail->Body    = $this->getBody('acceptReservationFinal', [
                'whenFrom' => $whenFrom->getEmailDateTimeFormat(),
                'whenTo' => $whenTo->getEmailDateTimeFormat(),
                'password' => $hashKey
            ]
        );
        return $this->sendMail();
    }

    public function sendDeletedReservation(string $email, DateTime $from, DateTime $to): bool
    {
        $whenFrom = new DateTimeUtil($from);
        $whenTo = new DateTimeUtil($to);

        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o zrušení rezervace do učebny esportu FIM';
        $this->mail->Body    = $this->getBody('deleteReservation', [
                'whenFrom' => $whenFrom->getEmailDateTimeFormat(),
                'whenTo' => $whenTo->getEmailDateTimeFormat()
            ]
        );
        return $this->sendMail();
    }

    public function sendBlackListInfo(string $email, string $reason): bool
    {
        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Informac o zamezení přístupu do esport učebny';
        $this->mail->Body    = $this->getBody('blackList', ['reason' => $reason]);
        return $this->sendMail();
    }

    public function passwordRenewal(string $email): bool
    {
        $this->mail->addAddress($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Žádost o změnu hesla do rezervačního systému esport učebny';
        $this->mail->Body    = $this->getBody('passwordRenewal', ['email' => $email, 'encoded' => base64_encode($email)]);
        return $this->sendMail();
    }

    public function sendReservationAskApplicantMail(array $data): bool
    {
        $whenFrom = new DateTimeUtil($data['dateWhen']);
        $whenTo = clone $whenFrom;

        $fromTime = explode(':', $data['timeFrom']);
        $whenFrom->setTime($fromTime[0], $fromTime[1]);

        $toTime = explode(':', $data['timeTo']);
        $whenTo->setTime($toTime[0], $toTime[1]);

        $this->mail->addAddress($data['email']);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Přijetí žádosti o rezervaci celé učebny';
        $this->mail->Body    = $this->getBody('applicantAsk', [
                'timeFrom' => $whenFrom->getEmailDateTimeFormat(),
                'timeTo' => $whenTo->getEmailDateTimeFormat()
            ]
        );
        return $this->sendMail();
    }
}