<?php
namespace Classes;

use DateInterval;
use DateTime;

class DateTimeUtil extends DateTime
{
    const MONTHS = [
        1 => 'Leden',
        2 => 'Únor',
        3 => 'Březen',
        4 => 'Duben',
        5 => 'Květen',
        6 => 'Červen',
        7 => 'Červenec',
        8 => 'Srpen',
        9 => 'Září',
        10 => 'Říjen',
        11 => 'Listopad',
        12 => 'Prosinec',
    ];

    const MONTHS_REVERSE = [
        'Leden' => 1,
        'Únor' => 2,
        'Březen' => 3,
        'Duben' => 4,
        'Květen' => 5,
        'Červen' => 6,
        'Červenec' => 7,
        'Srpen' => 8,
        'Září' => 9,
        'Říjen' => 10,
        'Listopad' => 11,
        'Prosinec' => 12,
    ];

    public function __construct($date = '')
    {
        parent::__construct(($date instanceof DateTime)? $date->format('Y-m-d H:i:s') : $date);
    }

    public function getEmailDateTimeFormat() : string
    {
        return $this->format('H:i d.m.Y');
    }

    public function getDateTimeFormat() : string
    {
        return $this->format('Y.m.d H:i:s');
    }

    public function getDateFormat() : string
    {
        return $this->format('Y.m.d');
    }

    public function getDateFormFormat() : string
    {
        return $this->format('Y-m-d');
    }

    public function getTimeFormat() : string
    {
        return $this->format('H:i');
    }

    public static function getAllMonths() : array
    {
        return self::MONTHS;
    }

    public static function getMonth($month) : string
    {
        return self::MONTHS[$month];
    }

    public function getCalendarDateFormat(): string
    {
        return $this->format('Y-m-d\TH:i:s');
    }

    public function day(): string
    {
        return $this->format('d');
    }

    public function hour(): string
    {
        return $this->format('H');
    }

    public function minute(): string
    {
        return $this->format('i');
    }

    public function getCompareFormat(): string
    {
        return $this->format('YmdHis');
    }

    public function getWeekNumber() : int
    {
        return $this->format('W');
    }

    public function getHourDiff(DateTimeUtil $to) : float
    {
        $hours = (int) $to->format('H') - $this->format('H');
        $minutes = (int) $to->format('i') - $this->format('i');

        return $hours + ($minutes >= 30 ? 0.5 : 0);
    }

    public function getMyWeek() {
        $startOfWeek = clone $this;
        $startOfWeek->modify('this week');

        $endOfWeek = clone $startOfWeek;
        $endOfWeek->modify('+6 days');
        return [
            'start' => $startOfWeek,
            'end' => $endOfWeek
        ];
    }
}