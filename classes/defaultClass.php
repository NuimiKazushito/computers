<?php
namespace Classes;

use Classes\Login\Manager;
use Presenters\Container;
use ReflectionClass;
use Tools\Inflector;

class DefaultClass
{
    const ALERT_ERROR = 'danger';
    const ALERT_SUCCESS = 'success';

    const LATTE_ADMIN = 'admin';
    const LATTE_USER = 'user';
    const LATTE_MAINTENANCE = 'maintenance';


    const M_HOME = 1;
    const M_USER = 2;
    const M_INFORMATION = 3;
    const M_GAMES = 4;
    const M_RESERVATION_ASK = 5;
    const M_COMPUTERS = 6;
    const M_DASHBOARD = 7;
    const M_BLACK_LIST = 8;
    const M_TEAM = 9;
    const M_CRON = 10;
    const M_RULES = 11;
    const M_CALENDAR = 12;

    const MODAL_RESERVATION = 1;
    const MODAL_ADMIN = 2;
    const MODAL_INFORMATION = 3;
    const MODAL_GAMES = 4;
    const MODAL_COMPUTERS = 5;
    const MODAL_BLACK_LIST = 6;
    const MODAL_TEAM = 7;
    const MODAL_USER = 8;
    const MODAL_RULE = 9;

    public ?Container $container;

    protected string $view;

    protected ReflectionClass $reflection;

    protected array $template;

    protected bool $refreshMessage = false;

    public function __construct(Container $container = null)
    {
        $this->container = $container;
    }

    public static function getInstance($container = null)
    {
        static $instance = null;
        if ($instance === null)
        {
            $instance = new static($container);
            $instance->reflection = new \ReflectionClass($instance);
        }
        return $instance;
    }

    public function init($args)
    {
        unset($args[0]);
        if(array_key_exists(2, $args))
        {
            unset($args[1]);
        }
        $view = strtolower(array_shift($args));

        if ($view == '')
        {
            $view = 'default';
        }

        $this->view = strtr($view, ['-' => '_']);
        $methodName = 'render' . Inflector::camelize($view);

        if ($this->reflection->hasMethod($methodName))
        {
            $result = $this->reflection->getMethod($methodName)->invokeArgs($this, $args);
            echo $result;

            exit;
        } else {
            if ($this->reflection->hasMethod('renderDefault'))
            {
                $result = $this->reflection->getMethod('renderDefault')->invokeArgs($this, $args);
                echo $result;

                exit;
            }
        }
    }

    /**
     * Navraci parametr z GET / POST
     * pokud je zadán klíč, vrátí pouze danou hodnotu
     * pokud není, vratí vše jak v POST tak i v GET
     *
     */
    public function getParameter(string $key = null) : mixed
    {
        $parameters = array_merge($_POST, $_GET);

        if ($key)
        {
            foreach ($parameters as $paramKey => $data)
            {
                if($key == $paramKey)
                {
                    return $data;
                }
            }
        }

        return $parameters;
    }

    private function getDefaultData(): void
    {
        $this->template['isLogged'] = UserData::checkLoggedIn();
        $this->template['isUnderMaintenance'] = StaticFunctions::isUnderMaintenance();
        if (!array_key_exists('messages', $this->template))
        {
            $this->template['messages'] = $this->getMessage();
        }
        $this->template['userData'] = null;

        if ($this->template['isLogged'])
        {
            $this->template['userData'] = $this->container->getLoginManager()->getAllByLogin(UserData::getUserName());
            $this->template['userData']['permName'] = Manager::PERMISSION_DICTIONARY[UserData::getPermission()];
        }
    }

    private function getMessage() : array
    {
        $return = [];

        if (array_key_exists('alert', $_SESSION))
        {
            foreach ($_SESSION['alert'] as $message)
            {
                $data['severity'] = $message['severity'];
                $data['message'] = $message['message'];
                $data['title'] = $message['title'];
                $return[] = $data;
            }
        }

        unset($_SESSION['alert']);
        return $return;
    }

    public function renderLayout(string $file, string $specificLatte = '', bool $checkMaintenance = true) : void
    {
        if (StaticFunctions::isUnderMaintenance() && $checkMaintenance)
        {
            if (UserData::isUser() || !UserData::checkLoggedIn())
            {
                StaticFunctions::setHeader('/home/maintenance/');
            }
        }

        $reflection = new ReflectionClass($this);

        $latte = new \Latte\Engine;
        $latte->setTempDirectory('cache');

        $latte->addFilter('translate', function(string $s)
            {
                return $s;
            }
        );


        $this->getDefaultData();

        $this->isSelect2Used();
        $template = $this->getData();


        $url = sprintf('www/%s/%s.latte', $specificLatte ?: strtolower($reflection->getName()), $file);

        if($this->refreshMessage)
        {
            $template['messages'] = $this->getMessage();
        }

        if(UserData::isAdmin())
        {
            $template['askNumber'] = $this->container->getReservationAskManager()->getNumber([\Classes\ReservationAsk\Manager::STATE => \Classes\ReservationAsk\Manager::UNSOLVED]);
            $template['toConfirm'] = $this->container->getReservationAllManager()->getNumber([\Classes\ReservationAll\Manager::CONFIRMED => \Classes\ReservationAll\Manager::C_TO_PROCESS]);
            $template['blacklisted'] = $this->container->getBlackListManager()->getNumber([\Classes\Blacklist\Manager::STATE => \Classes\Blacklist\Manager::BLACKLISTED]);
        }
        $latte->render($url, $template);
    }

    public function addData(string $key, $data) : void
    {
        $this->template[$key] = $data;
    }

    public function getData(string $key = null)
    {
        if ($key)
        {
            return $this->template[$key];
        } else {
            return $this->template;
        }
    }

    public function isSelect2Used(bool $used = false) : void
    {
        if (!array_key_exists('select2', $this->getData()))
        {
            $this->addData('select2', $used);
        }
    }

    public function setActiveMenu(int $menuID) : void
    {
        $this->addData('activeMenu', $menuID);
    }

    public function setActiveModal(int $modalID) : void
    {
        $this->addData('modal', $modalID);
    }

    public function setRefreshMessage(bool $refreshMessage) : void
    {
        $this->refreshMessage = $refreshMessage;
    }

    public function isLoggedIn(bool $checkIfAdmin = true) : void
    {
        if (!UserData::checkLoggedIn())
        {
            StaticFunctions::addErrorMessage('Nejste přihlášen');
            StaticFunctions::setHeader('/');
            exit;
        }

        if ($checkIfAdmin && !UserData::isAdmin())
        {
            StaticFunctions::addErrorMessage('Nemáte oprávnění do admin sekce');
            StaticFunctions::setHeader('/');
        }

        if ($this->container->getBlackListManager()->onBlacklist(UserData::getEmail()))
        {
            StaticFunctions::addErrorMessage('Nacházíte se na černé listině');
            StaticFunctions::setHeader('/user/logout?error=1');
        }
    }
}
