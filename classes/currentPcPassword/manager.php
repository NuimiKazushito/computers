<?php

namespace Classes\CurrentPcPassword;

use Classes\DateTimeUtil;
use Classes\Dibi;
use Dibi\Exception;

class Manager extends Dibi
{
    const TABLE = 'tCurrentPcPassword';
    const PRIMARY = 'id';

    const FK_RESERVATION_ID = 'FK_reservationID';
    const CREATE_DT = 'createDT';
    const END_DT = 'endDT';
    const PC_ID = 'pcID';
    const CURRENT_PASSWORD = 'currentPassword';

    public function deleteByComputerID(int $computerID): bool
    {
        try {
            $this->db->delete(self::TABLE)
                ->where('%n = %i', self::PC_ID, $computerID)
                ->execute();
            return true;
        } catch (Exception $e) {
            bdump($e);
            return false;
        }

    }

    public function updateByComputerID(int $computerID, string $password,  DateTimeUtil|\Dibi\DateTime  $to, int $resID): bool
    {
        try {
            $this->db->update(self::TABLE, [
                self::CURRENT_PASSWORD => $password,
                self::FK_RESERVATION_ID => $resID,
                self::END_DT => $to,
            ])
                ->where('%n = %i', self::PC_ID, $computerID)
                ->execute();
            return true;
        } catch (Exception $e) {
            bdump($e);
            return false;
        }

    }

    public function isPasswordActual(int $pcID, string $password): bool
    {
        $query = $this->db->select(self::PRIMARY);
        $query->from(self::TABLE);
        $query->where('%n = %i', self::PC_ID, $pcID)
            ->where('%n = %s', self::CURRENT_PASSWORD, $password);

        return (bool)$query->fetch();
    }
}