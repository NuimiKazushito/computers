<?php

namespace Classes\Login;

use \Classes\User\Manager as UManager;

class Manager extends \Classes\Dibi
{
    const TABLE = 'tLogin';
    const PRIMARY = 'id';

    const FK_USER_ID = 'FK_userID';
    const USER_NAME = 'userName';
    const PASSWORD = 'password';
    const PASSWORD_ENCRYPT = 'passwordEncrypt';
    const PERMISSION = 'permission';

    const GOD = 0;
    const ADMIN = 1;
    const TEAM_LEADER = 2;
    const USER = 3;

    const PERMISSION_DICTIONARY = [
        self::GOD => 'Správce',
        self::ADMIN => 'Admin',
        self::TEAM_LEADER => 'Velitel týmu',
        self::USER => 'Uživatel'
    ];

    public function getAllByLogin($login): bool|array
    {
        $query = $this->db->select(self::PASSWORD);
        $query->select(self::PASSWORD_ENCRYPT);
        $query->select(self::USER_NAME);
        $query->select(self::PERMISSION);
        $query->select(self::FK_USER_ID);
        $query->select(UManager::EMAIL);
        $query->from(self::TABLE)->as('l');
        $query->leftJoin(UManager::TABLE)->as('u')->on('l.%n = u.%n',
            self::FK_USER_ID, UManager::PRIMARY);
        $query->where('l.%n = %s OR u.%n = %s',
            self::USER_NAME, $login,
            UManager::EMAIL, $login);

        return $this->getFirst($query);
    }

    public function alreadyExist(string $username): bool
    {
        $query = $this->db->select(1);
        $query->from(self::TABLE);
        $query->where('%n = %s', self::USER_NAME, $username);

        return (bool)$query->fetchSingle();
    }

    public function isEmailAlreadyUsed(string $email): bool
    {
        $query = $this->db->select(1);
        $query->from(self::TABLE)->as('l');
        $query->leftJoin(UManager::TABLE)->as('u')->on('l.%n = u.%n',
            self::FK_USER_ID, UManager::PRIMARY);
        $query->where('LOWER(u.%n) = %s',
            UManager::EMAIL, $email);

        return (bool) $query->fetch();
    }

    public function getAllByUserId(int $userId): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);
        $query->where('%n = %i', self::FK_USER_ID, $userId);

        return (array) $query->fetchAll()[0];
    }
}