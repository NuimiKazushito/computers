<?php

namespace Classes\BlackList;

use Classes\Dibi;
use Dibi\Exception;

class Manager extends Dibi
{
    const TABLE = 'tBlackList';
    const PRIMARY = 'id';

    const EMAIL = 'email';
    const REASON = 'reason';
    const DATE = 'endDate';
    const STATE = 'state';

    const BLACKLISTED = 1;
    const NOT_BLACKLISTED = 0;

    public function setFree(int $userID): bool
    {
        try {
            $this->db->update(self::TABLE, [
                    self::STATE => self::NOT_BLACKLISTED
                ]
            )
                ->where('%n = %i', self::PRIMARY, $userID)
                ->execute();
            return true;
        } catch (Exception $e) {
            bdump($e);
            return false;
        }
    }

    public function onBlacklist(string $email): bool
    {
        $query = $this->db->select(self::STATE);
        $query->from(self::TABLE);
        $query->where('%n = %s', self::EMAIL, $email);
        return $query->fetchSingle() == self::BLACKLISTED;
    }
}