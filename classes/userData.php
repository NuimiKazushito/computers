<?php

namespace Classes;

use Classes\Login\Manager as LManager;
use Classes\team\Team;

class UserData
{
    public function __construct(string $email, int $id, int $permission, string $userName)
    {
        $_SESSION['email'] = $email;
        $_SESSION['userID'] = $id;
        $_SESSION['permission'] = $permission;
        $_SESSION['userName'] = $userName;
        $_SESSION['admin'] = in_array(self::getPermission(), [LManager::ADMIN, LManager::GOD]);
    }

    public static function getEmail(): ?string
    {
        return (array_key_exists('email', $_SESSION))? $_SESSION['email'] : null;
    }

    public static function getUserID() : ?int
    {
        $id =  (array_key_exists('userID', $_SESSION))? $_SESSION['userID'] : null;

        if ($id == 1) {
            StaticFunctions::setHeader('/user/logout?sorry=true');
            return null;
        } else {
            return $id;
        }
    }

    public static function getPermission() : ?int
    {
        return (array_key_exists('permission', $_SESSION))? $_SESSION['permission'] : null;
    }

    public static function getUserName() : string
    {
        return (array_key_exists('userName', $_SESSION))? $_SESSION['userName'] : '';
    }

    public static function isAdmin() : bool
    {
        return (array_key_exists('admin', $_SESSION))? $_SESSION['admin'] : false;
    }

    public static function isUser(): bool
    {
        return self::checkLoggedIn() && !in_array(self::getPermission(), [LManager::GOD, LManager::ADMIN]);
    }

    public static function checkLoggedIn() : bool
    {
        return array_key_exists('userName', $_SESSION);
    }

    public static function getTeam() : ?Team
    {
        return (array_key_exists('team', $_SESSION))? $_SESSION['team'] : null;
    }

    public static function setTeam(string $name, int $id, bool $leader): void
    {
        $_SESSION['team'] = new Team($name, $id, $leader);
    }
    public static function disbandTeam(): void
    {
        $_SESSION['team'] = null;
    }

    public static function getAll(): array
    {
        return [
            'id' => self::getUserID(),
            'email' => self::getEmail(),
            'permission' => self::getPermission(),
            'userName' => self::getUserName(),
            'isAdmin' => self::isAdmin(),
            'isUser' => self::isUser()
        ];
    }
}