<?php

namespace Classes\ReservationAsk;

use Classes\Dibi;
use Classes\Reservation\Manager as RManager;

class Manager extends Dibi
{
    const TABLE = 'tReservationAsk';
    const PRIMARY = 'id';

    const EMAIL = 'email';
    const PHONE = 'phone';
    const NAME = 'name';
    const DATE_WHEN = 'dateWhen';
    const TIME_FROM = 'timeFrom';
    const TIME_TO = 'timeTo';
    const REASON = 'reason';
    const STATE = 'state';

    const SOLVED = 1;
    const UNSOLVED = 0;

    public function getAllActive(): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);
        $query->where('%n = %i', self::STATE, self::UNSOLVED);

        return $query->fetchAll();
    }

    public function setSolved(int $reservationID)
    {
        $this->db->update(self::TABLE, [
               self::STATE => self::SOLVED
                ]
            )
            ->where('%n = %i', self::PRIMARY, $reservationID)
            ->execute();
    }
}