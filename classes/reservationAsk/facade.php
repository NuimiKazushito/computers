<?php

namespace Classes\ReservationAsk;

use Classes\DateTimeUtil;
use Classes\ReservationAsk\Manager as RAskManager;
use Classes\Reservation\Manager as RManager;

class Facade
{
    private RAskManager $reservationAskManager;
    private RManager $reservationManager;

    public function __construct(RAskManager $reservationAskManager, RManager $reservationManager)
    {
        $this->reservationAskManager = $reservationAskManager;
        $this->reservationManager = $reservationManager;
    }

    public function getAllActive(): array
    {
        $askReservations = $this->reservationAskManager->getAllActive();
        $allReservation = $this->reservationManager->getAllAfterToday();

        foreach ($askReservations as $activeKey => $activeData)
        {
            $from = new DateTimeUtil($activeData[RAskManager::DATE_WHEN]);
            $from->setTime($activeData[RAskManager::TIME_FROM]->h, $activeData[RAskManager::TIME_FROM]->i);
            $to = new DateTimeUtil($activeData[RAskManager::DATE_WHEN]);
            $to->setTime($activeData[RAskManager::TIME_TO]->h, $activeData[RAskManager::TIME_TO]->i);

            $acceptable = count($allReservation) > 0 ? 0 : 1;
            foreach ($allReservation as $reservationData)
            {
                if ($from >= $reservationData[RManager::TIME_TO] ||
                    $to <= $reservationData[RManager::TIME_FROM])
                {
                    $acceptable = 1;
                }
            }

            $askReservations[$activeKey]['acceptable'] = $acceptable;
        }

        return $askReservations;
    }
}