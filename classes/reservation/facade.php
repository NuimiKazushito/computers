<?php

namespace Classes\Reservation;

use Classes\DateTimeUtil;
use Classes\Reservation\Manager as RManager;
use Classes\ReservationAll\Manager as RAManager;
use Classes\ReservationAsk\Manager as RAskManager;
use Classes\Login\Manager as LManager;
use Classes\StaticFunctions as SF;
use Classes\User\Manager as UManager;
use Classes\CurrentPcPassword\Manager as CPPManager;
use Classes\Super\Paginator;
use Classes\UserData;

class Facade
{
    private RManager $reservationManager;
    private RAManager $reservationAllManager;
    private RAskManager $reservationAskManager;
    private UManager  $userManager;
    private CPPManager $currentPcPasswordManager;
    private Paginator $paginator;

    public function __construct(RManager $reservationManager, RAManager $reservationAllManager, RAskManager $reservationAskManager,
                                UManager $userManager, CPPManager $currentPcPasswordManager, Paginator $paginator)
    {
        $this->reservationManager = $reservationManager;
        $this->reservationAllManager = $reservationAllManager;
        $this->reservationAskManager = $reservationAskManager;
        $this->userManager = $userManager;
        $this->currentPcPasswordManager = $currentPcPasswordManager;
        $this->paginator = $paginator;
    }

    public function getAllOneTimeReservations(?string $search, $type = 0, $html = false): array|string
    {
        if ($type)
        {
            $data = $this->reservationAllManager->getAllReservations();
            $template = 'reservationsAll';
        } else {
            $data = $this->reservationManager->getAllOneTime($search, true);
            if (UserData::isUser()) {
                $template = 'userReservations';
            } else {
                $template = 'reservations';
            }
        }

        foreach ($data as $key => $allOTData)
        {
            $dateFrom = new DateTimeUtil($allOTData[RManager::TIME_FROM]);
            $dateTo = new DateTimeUtil($allOTData[RManager::TIME_TO]);
            $data[$key]['date'] = $dateFrom->getDateFormat();
            $data[$key]['from'] = $dateFrom->getTimeFormat();
            $data[$key]['to'] = $dateTo->getTimeFormat();
        }

        if (!$type)
        {
            $myData = $data;
            $data = [];
            foreach ($myData as $reservationData)
            {
                $data[$reservationData['computerID']][] = $reservationData;
            }
        }

        if ($html)
        {
            return SF::renderLatte($template, [
                    'otReservation' => $data,
                    'searchString' => $search
                ]
            );
        } else {
            return $data;
        }
    }

    public function getAllReservationsToCalendar(int $computerID = 1, bool $pc = false): array
    {
        $data = $this->reservationManager->getCalendarReservations($computerID);

        $return = [];
        foreach ($data as $calendarData)
        {
            $dateFrom = new DateTimeUtil($calendarData[RManager::TIME_FROM]);
            $dateTo = new DateTimeUtil($calendarData[RManager::TIME_TO]);
            $return[] = [
                'title' => $calendarData[UManager::FIRST_NAME] . ' ' . $calendarData[UManager::SECOND_NAME] .
                    ($pc ? sprintf(' (PC %s)', $calendarData[RManager::FK_COMPUTER_ID]) : ''),
                'start' => $dateFrom->getCalendarDateFormat(),
                'end' => $dateTo->getCalendarDateFormat(),
            ];
        }

        return $return;
    }

    public function getHashKey(): string
    {
        while(true)
        {
            $checker = 0;
            $hashKey = SF::passwordEncrypt(5);

            if (!preg_match('/[A-Z]/', $hashKey))
            {
                $checker++;
            }
            if (!preg_match('/[a-z]/', $hashKey))
            {
                $checker++;
            }
            if (!preg_match('/[0-9]/', $hashKey))
            {
                $checker++;
            }

            if(! $this->reservationManager->doesKeyExist($hashKey) && $checker == 0)
            {
                break;
            }
        }
        return $hashKey;
    }

    public function useKey($hashKey, $computerID) : bool
    {
        if ($this->reservationManager->canUseKey($hashKey, $computerID))
        {
            $this->reservationManager->useKey($hashKey, $computerID);
            return true;
        }
        return false;
    }

    public function checkPassword(int $resID, DateTimeUtil $dateFrom, DateTimeUtil $dateTo, string $password, int $pcID, string $user): void
    {
        $now = new DateTimeUtil();
        $nowUP = clone $dateFrom;
        $nowUP->modify('+30 minutes');

        if ($now->getCompareFormat() > $dateFrom->getCompareFormat() && $now->getCompareFormat() < $nowUP->getCompareFormat())
        {
            if (!$this->currentPcPasswordManager->isPasswordActual($pcID, $password))
            {
                $this->changePasswordPS($user, $password);
                $this->currentPcPasswordManager->updateByComputerID($pcID, $password, $dateTo, $resID);
            }
        }
    }

    public function changePasswordPS(string $name, string $password): void
    {
        if ($password != \Classes\ReservationAll\Manager::TO_PROCESS)
        {
            $psscriptpath = __DIR__ . sprintf("/../../scripts/changepwd.ps1 %s %s", $name, $password);
            shell_exec("powershell.exe -executionpolicy remotesigned -File " . $psscriptpath);
        }
    }

    public function acceptReservation(int $reservationID): array
    {
        $data = $this->reservationAskManager->getAllByPrimary($reservationID);

        $name = explode(' ', $data[RAskManager::NAME]);
        $userData[UManager::EMAIL] = $data[RAskManager::EMAIL];
        $userData[UManager::PHONE] = $data[RAskManager::PHONE];
        $userData[UManager::FIRST_NAME] = $name[0];
        $userData[UManager::SECOND_NAME] = array_key_exists(1, $name) ? $name[1] : '';
        $userID = $this->userManager->managerInsert($userData, true);

        $whenFrom = new DateTimeUtil($data[RAskManager::DATE_WHEN]);
        $whenFrom->setTime($data[RAskManager::TIME_FROM]->h, $data[RAskManager::TIME_FROM]->i);
        $whenTo = new DateTimeUtil($data[RAskManager::DATE_WHEN]);
        $whenTo->setTime($data[RAskManager::TIME_TO]->h, $data[RAskManager::TIME_TO]->i);

        $hashKey = RAManager::TO_PROCESS;
        for ($i = 1; $i < 11; $i++)
        {
            $dataReservation = [];
            $dataReservation[RManager::FK_USER_ID] = $userID;
            $dataReservation[RManager::CREATE_DT] = new DateTimeUtil();
            $dataReservation[RManager::HASH_KEY] = $hashKey;
            $dataReservation[RManager::ONE_TIME] = 1;

            $dataReservation[RManager::FK_COMPUTER_ID] = $i;
            $dataReservation[RManager::TIME_FROM] = $whenFrom;
            $dataReservation[RManager::TIME_TO] = $whenTo;

            $this->reservationManager->managerInsert($dataReservation);
        }

        $dataReservationAll = [];
        $dataReservationAll[RAManager::FK_USER_ID] = $userID;
        $dataReservationAll[RAManager::CREATE_DT] = new DateTimeUtil();
        $dataReservationAll[RAManager::TIME_FROM] = $whenFrom;
        $dataReservationAll[RAManager::TIME_TO] = $whenTo;
        $dataReservationAll[RAManager::HASH_KEY] = $hashKey;
        $dataReservationAll[RAManager::CONFIRMED] = 0;
        $this->reservationAllManager->managerInsert($dataReservationAll);

        $this->reservationAskManager->setSolved($reservationID);

        return [
            'email' => $data['email'],
            'whenFrom' => $whenFrom,
            'whenTo' => $whenTo
        ];
    }

    public function getReportData(int $page, ?DateTimeUtil $from, ?DateTimeUtil $to, ?string $email, bool $html = false): array|string
    {
        $reportQuery = $this->reservationManager->getDataForReport($from, $to, $email, false);

        $this->paginator->setQueryLimit($reportQuery, $this->paginator->getOffset($page));
        $reportData = $reportQuery->fetchAll();

        foreach ($reportData as $key => $report)
        {
            $timeFrom = new DateTimeUtil($report['timeFrom']);
            $reportData[$key]['timeFrom'] = $timeFrom->getDateTimeFormat();

            $timeTo = new DateTimeUtil($report['timeTo']);
            $reportData[$key]['timeTo'] = $timeTo->getDateTimeFormat();
            $reportData[$key]['timeCount'] = $timeFrom->getHourDiff($timeTo);
        }

        if ($html)
        {
            return SF::renderLatte('dashboard', [
                    'reportData' => $reportData,
                    'pages' =>  $this->paginator->getNumberOfPages(),
                    'page' =>  $page,
                    'ajax' => 'dashboardTable'
                ]
            );
        } else {
            return [
                'pages' =>  $this->paginator->getNumberOfPages(),
                'data' => $reportData
            ];
        }
    }

    public function getTeamReservationForm($date)
    {
        $this->reservationManager->getByDate();
    }

    public function logoutPC(string $name, string $pc): void
    {
        $psscriptpath = __DIR__ . sprintf("/../../scripts/logoffsession.ps1 %s %s", $name, $pc);
        shell_exec("powershell.exe -executionpolicy remotesigned -File " . $psscriptpath);
    }
}