<?php

namespace Classes\Reservation;

use Classes\DateTimeUtil;
use Classes\Login\Manager as LManager;
use Classes\ServerUsers\Manager as SUManager;
use Classes\StaticFunctions as SF;
use Classes\User\Manager as UManager;
use Classes\Computer\Manager as CManager;
use Classes\ReservationAll\Manager as RSManager;
use Classes\UserData;
use Dibi\Exception;

class Manager extends \Classes\Dibi
{
    const TABLE = 'tReservation';
    const PRIMARY = 'id';

    const FK_USER_ID = 'FK_userID';
    const FK_COMPUTER_ID = 'FK_computerID';
    const CREATE_DT = 'createDT';
    const TIME_FROM = 'timeFrom';
    const TIME_TO = 'timeTo';
    const HASH_KEY = 'hashKey';
    const ONE_TIME = 'oneTime';
    const USED = 'used';

//deleting
//DELETE FROM treservation
//WHERE id IN (
//SELECT r.id
//FROM treservation r
//LEFT JOIN tUser u ON r.FK_userID = u.id
//WHERE u.id IN (
//SELECT FK_userID
//FROM tLogin
//)
//)
    public function getAllOneTime(?string $search, bool $expectAll = false): array
    {
        $query = $this->db->select(self::TIME_FROM);
        $query->from(self::TABLE)->as('r');
        $query->select(self::TIME_TO)
            ->select(self::HASH_KEY)
            ->select(UManager::FIRST_NAME)
            ->select(UManager::SECOND_NAME)
            ->select(UManager::EMAIL)
            ->select(UManager::PHONE)
            ->select(self::FK_COMPUTER_ID)->as('computerID')
            ->select(CManager::NAME)
            ->select('r.%n', self::PRIMARY)->as('reservationID');
        $query->leftJoin(UManager::TABLE)->as('u')->on('r.%n = u.%n',
            self::FK_USER_ID, UManager::PRIMARY);
        $query->leftJoin(CManager::TABLE)->as('c')->on('r.%n = c.%n',
            self::FK_COMPUTER_ID, CManager::PRIMARY);
        $query->where('%n = %i', self::ONE_TIME, 1);

        if ($search)
        {
            $query->where('(%n LIKE %~like~ OR %n LIKE %~like~ OR %n LIKE %~like~)',
                UManager::EMAIL, $search,
                UManager::FIRST_NAME, $search,
                UManager::SECOND_NAME, $search
            );
        } else {
            $query->where('%n >= %d', self::TIME_FROM, new \DateTime());
        }

        if ($expectAll)
        {
            $query->where('%n NOT IN %sql',
                self::HASH_KEY,
                $this->db->select(RSManager::HASH_KEY)
                    ->from(RSManager::TABLE));
        }

        if (UserData::isUser())
        {
            $query->where('u.%n = %i', UManager::PRIMARY, UserData::getUserID());
        }
        $query->orderBy(self::FK_COMPUTER_ID);
        $query->orderBy(self::TIME_FROM)->desc();
//        dumpe($query->test());
        return $query->fetchAll();
    }

    public function getCalendarReservations(int $computerId): array
    {
        $query = $this->db->select(self::TIME_FROM)
            ->select(self::TIME_TO)
            ->select(UManager::FIRST_NAME)
            ->select(UManager::SECOND_NAME)
            ->select(self::FK_COMPUTER_ID);
        $query->from(self::TABLE)->as('r');
        $query->leftJoin(UManager::TABLE)->as('u')->on('r.%n = u.%n',
            self::FK_USER_ID, UManager::PRIMARY);

        if ($computerId < 11)
        {
            $query->where('%n = %i', self::FK_COMPUTER_ID, $computerId);
        }

        return $query->fetchAll();
    }

    /**
     * false = does not exist
     * true = does exist
     */
    public function doesKeyExist(string $hashKey): bool
    {
        $now = new DateTimeUtil();

        $query = $this->db->select(1);
        $query->from(self::TABLE);
        $query->where('%n = %s', self::HASH_KEY, $hashKey)
            ->where('%n > %dt', self::CREATE_DT, $now->modify('-3 month'));

        return !is_null($query->fetchSingle());
    }

    public function canUseKey($hashKey, $computerID) : bool
    {
        $query = $this->db->select(1);
        $query->from(self::TABLE);
        $query->where('%dt BETWEEN %n AND %n',
            new \DateTime(), self::TIME_FROM, self::TIME_TO);
        $query->where('%n = %s', self::HASH_KEY, $hashKey);
        $query->where('%n = 0', self::USED);
        $query->where('%n = %i', self::FK_COMPUTER_ID, $computerID);

        return !is_null($query->fetchSingle());
    }

    public function useKey($hashKey, $computerID)
    {
        $this->db->update(self::TABLE, [self::USED => 1])
            ->where('%n = %s', self::HASH_KEY, $hashKey)
            ->where('%n = %i', self::FK_COMPUTER_ID, $computerID)
            ->execute();
    }

    public function deleteOldNadUsed(bool $used): bool
    {
        $now = new DateTimeUtil();
        try {
            $this->db->delete(self::TABLE)
                ->where('%n = %i', self::USED, $used ? 1 : 0)
                ->where('DATEDIFF(%d, %n ) > 60', $now, self::TIME_TO)
                ->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteByHashKey($hashKey) : bool
    {
        try {
            $this->db->delete(self::TABLE)
                ->where('%n = %s', self::HASH_KEY, $hashKey)
                ->execute();
            return true;
        } catch (Exception $e) {
            bdump($e);
            return false;
        }
    }

    public function getAllAfterToday(): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);
        $query->where('%n >= %d', self::TIME_FROM, new DateTimeUtil());

        return $query->fetchAll();
    }

    public function getAllToCheck($end = false): array
    {
        $now = new DateTimeUtil();
        $next = clone $now;
        $now->modify('-5 minutes');
        $next->modify('+5 minutes');

        $query = $this->db->select(self::TIME_FROM)
            ->select(self::HASH_KEY)
            ->select(self::TIME_TO)
            ->select('r.%n', self::PRIMARY)->as('reservation')
            ->select('c.%n', CManager::PRIMARY)->as('computer')
            ->select('su.%n', SUManager::USER_NAME);
        $query->from(self::TABLE)->as('r');
        $query->leftJoin(CManager::TABLE)->as('c')->on('r.%n = c.%n',
                self::FK_COMPUTER_ID, CManager::PRIMARY)
            ->leftJoin(SUManager::TABLE)->as('su')->on('c.%n = su.%n',
                CManager::PRIMARY, SUManager::FK_COMPUTER_ID);

        if($end)
        {
            $query->where('%n BETWEEN %dt AND %dt',
                self::TIME_TO, $now, $next);
        } else {
            $query->where('%n = 0', self::USED)
                ->where('%n BETWEEN %dt AND %dt',
                    self::TIME_FROM, $now, $next);
        }

        return $query->fetchAll();
    }

    public function findAllByComputerAndDate(int $computerID, DateTimeUtil $dateFrom, DateTimeUtil $dateTo)
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);
        $query->where('((%n >= %dt AND %n <= %dt) OR (%n < %dt AND %n > %dt))',
            self::TIME_FROM, $dateFrom,
            self::TIME_TO, $dateTo,
            self::TIME_FROM, $dateTo,
            self::TIME_FROM, $dateFrom);

        if ($computerID != 11)
        {
            $query->where('%n = %i', self::FK_COMPUTER_ID, $computerID);
        }

        return $query->fetchAll();
    }

    public function getAllByUserID(int $userID)
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);
        $query->where('%n = %i', self::FK_USER_ID, $userID);

        return $query->fetchAll();
    }

    public function getDataForReport(?DateTimeUtil $from, ?DateTimeUtil $to, ?string $email, bool $onlyData = true): \Dibi\Fluent|array
    {
        $query = $this->db->select('u.%n', UManager::FIRST_NAME)
            ->select('u.%n', UManager::SECOND_NAME)
            ->select('u.%n', UManager::EMAIL)
            ->select('u.%n', UManager::PHONE)
            ->select('r.%n', self::TIME_FROM)
            ->select('r.%n', self::TIME_TO)
            ->select('r.%n', self::FK_COMPUTER_ID);
        $query->from(self::TABLE)->as('r')
            ->leftJoin(UManager::TABLE)->as('u')->on('r.%n = u.%n',
                self::FK_USER_ID, UManager::PRIMARY);

        if (!is_null($from) && !is_null($to))
        {
            $to->setTime(23, 59);
            $query->where('r.%n >= %dt', self::TIME_FROM, $from);
            $query->where('r.%n <= %dt', self::TIME_TO, $to);
        } else if (!is_null($from) && is_null($to)) {
            $to = clone $from;
            $to->setTime(23, 59);
            $query->where('r.%n >= %dt', self::TIME_FROM, $from);
            $query->where('r.%n <= %dt', self::TIME_TO, $to);
        } else if (!is_null($to)) {
            $to->setTime(23, 59);
            $query->where('r.%n <= %dt', self::TIME_TO, $to);
        }

        if (!is_null($email)){
            $query->where('u.%n = %s', UManager::EMAIL, $email);
        }


        if (UserData::isUser())
        {
            $query->where('u.%n = %i', UManager::PRIMARY, UserData::getUserID());
        }
        
        $query->orderBy(self::TIME_FROM)->desc();

        if ($onlyData)
        {
            return $query->fetchAll();
        } else {
            return $query;
        }
    }

    public function doesHaveReservation(DateTimeUtil $from, DateTimeUtil $to, string $email): bool
    {
        $query = $this->db->select(1);
        $query->from(self::TABLE)->as('r')
            ->leftJoin(UManager::TABLE)->as('u')->on('r.%n = u.%n',
                self::FK_USER_ID, UManager::PRIMARY)
            ->leftJoin(LManager::TABLE)->as('l')->on('u.%n = l.%n',
                UManager::PRIMARY, LManager::FK_USER_ID);
        $query->where('((%dt >= %n AND %dt < %n) OR (%dt > %n AND %dt <= %n) OR (%dt = %n AND %dt = %n))',
            $from, self::TIME_FROM, $from, self::TIME_TO,
            $to, self::TIME_FROM, $to, self::TIME_TO,
            $from, self::TIME_FROM, $to, self::TIME_TO);
//        $query->where('((%n <= %d) AND (%n >= %d))',
//            self::TIME_FROM, $to, self::TIME_TO, $from);
        $query->where('u.%n = %s', UManager::EMAIL, $email);
        $query->where("l.%n != %i", LManager::PERMISSION, LManager::GOD);

        return (bool)$query->fetch();
    }

    public function getByDate(DateTimeUtil $date)
    {
        $from = $date;
        $from->setTime(0,0);
        $to = clone $date;
        $to->setTime(23,59);
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);
        $query->where('%n BETWEEN %dt AND %dt', self::TIME_FROM, $from, $to);

        return $query->fetchAll();
    }
}