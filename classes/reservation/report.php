<?php

namespace Classes\Reservation;

use Classes\DateTimeUtil;
use Classes\Super\ExcelGenerator as EGenerator;
use Classes\Reservation\Manager as RManager;
use Classes\User\Manager as UManager;
use Classes\StaticFunctions as SF;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Report
{

    private EGenerator $generator;
    private RManager $reservationManager;

    public function __construct(RManager $reservationManager)
    {
        $this->generator = new EGenerator();
        $this->reservationManager = $reservationManager;
    }

    public function generateAttendanceReport() : void
    {
        $sheet = $this->generator->getSheet();
        $spreadsheet = $this->generator->getSpreadSheet();

        //Head

        $this->generator->setDefaultHeader($spreadsheet, $sheet, 'Vygenerovaný report');
        $this->generator->setDefaultTime($sheet);

        $sheet->getStyle('B1:G2')->applyFromArray($this->generator->getBorderStyleHeader());
        $column = 'B';
        $row = '3';
        $sheet->setCellValue($column.$row, 'Kdo');
        $sheet->setCellValue(++$column.$row, 'Email');
        $sheet->setCellValue(++$column.$row, 'Telefon');
        $sheet->setCellValue(++$column.$row, 'Datum od');
        $sheet->setCellValue(++$column.$row, 'Datum do');
        $sheet->setCellValue(++$column.$row, 'Počítač');

        $sheet->getStyle('B3:G3')->applyFromArray($this->generator->getBorderStyleLabels());

        //Body
        $column = 'B';
        $row = '4';

        $from = SF::getFilter() && SF::getFilter()->getFrom() ? SF::getFilter()->getFrom() : null;
        $to = SF::getFilter() && SF::getFilter()->getTo() ? SF::getFilter()->getTo() : null;
        $email = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        $data = $this->reservationManager->getDataForReport($from, $to, $email);

        $lastColumn = (3 + count($data));
        $sheet->getStyle('C4:CH'. $lastColumn)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G4:G'. $lastColumn)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('B4:G'. $lastColumn)->applyFromArray($this->generator->getBorderStyleBody());
        foreach ($data as $reservation)
        {
            $from = new DateTimeUtil($reservation[RManager::TIME_FROM]);
            $to = new DateTimeUtil($reservation[RManager::TIME_TO]);

            $sheet->setCellValue($column.$row, sprintf('%s %s', $reservation[UManager::FIRST_NAME], $reservation[UManager::SECOND_NAME]));
            $sheet->setCellValue(++$column.$row, $reservation[UManager::EMAIL]);
            $sheet->setCellValue(++$column.$row, $reservation[UManager::PHONE]);
            $sheet->setCellValue(++$column.$row, $from->getEmailDateTimeFormat());
            $sheet->setCellValue(++$column.$row, $to->getEmailDateTimeFormat());
            $sheet->setCellValue(++$column.$row, $reservation[RManager::FK_COMPUTER_ID]);
            $row++;
            $column = 'B';
        }

        $now = new DateTimeUtil();
        $this->generator->save($now->getDateFormat() . '_Rezervace');
    }
}