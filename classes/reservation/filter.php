<?php

namespace Classes\Reservation;

use Classes\DateTimeUtil;

class Filter
{
    private ?DateTimeUtil $from;
    private ?DateTimeUtil $to;
    private ?string $email;

    public function __construct(?DateTimeUtil $from, ?DateTimeUtil $to, ?string $email)
    {
        $this->from = $from;
        $this->to = $to;
        $this->email = $email;
    }

    public function getFrom(): ?DateTimeUtil
    {
        return $this->from ?? null;
    }

    public function getTo(): ?DateTimeUtil
    {
        return $this->to ?? null;
    }

    public function getSearchString(): ?string
    {
        return $this->email ?? null;
    }
}