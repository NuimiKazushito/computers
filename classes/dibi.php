<?php
namespace Classes;

use Dibi\Connection;
use Dibi\Fluent;
use ReflectionClass;

class Dibi
{
    const RETURN_INSERTED_ID = 'n';
    const TABLE = null;
    const PRIMARY = null;
    const ALL = '*';
    const DESC = 'DESC';
    const ASC = 'ASC';

    public $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getAllByPrimary(int $id) : array
    {
        $query = $this->db->select('*');
        $query->from($this::TABLE);
        $query->where('%n = %i', $this::PRIMARY, $id);
        return (array) $query->fetch();
    }

    public function getAll(string $associated = '', array $where = []) : array
    {
        $query = $this->db->select('*');
        $query->from($this::TABLE);

        if ($where)
        {
            $query->where($where);
        }

        if ($associated !== '')
        {
            return $query->fetchAssoc($associated);
        } else {
            return $query->fetchAll();
        }
    }

    public function getNewID() : int
    {
        $query = $this->db->select($this::PRIMARY);
        $query->from($this::TABLE);
        $query->orderBy('%n DESC', $this::PRIMARY);
        $query->limit(1);

        $return = $query->fetchSingle();

        return $return ? $return + 1 : 1;
    }

    public function managerInsert(array $data, bool $returnKey = false) : int|bool
    {
        try {
            if ($returnKey)
            {
                return $this->db->insert($this::TABLE, $data)->execute(self::RETURN_INSERTED_ID);
            }  else {
                $this->db->insert($this::TABLE, $data)->execute();
                return true;
            }
        } catch(\Exception $e) {
            return false;
        }
    }

    public function managerUpdate(array $data, int $id) : bool
    {
        $query = $this->db->update($this::TABLE, $data)
            ->where('%n = %i', $this::PRIMARY, $id);
        $query->execute();
        return true;
    }

    public function getFirst(Fluent $query) : array|false
    {
        if ($query->fetchAll())
        {
            return (array) $query->fetchAll()[0];
        }
        return false;
    }

    public function deleteByPrimary($id) : bool
    {
        $this->db->delete($this::TABLE)
            ->where('%n = %i', $this::PRIMARY, $id)
            ->execute();
        return true;
    }

    public function getNumber(array $where = [])
    {
        $query = $this->db->select('COUNT(%n)', $this::PRIMARY)->as('count');
        $query->from($this::TABLE);

        if(!empty($where))
        {
            $query->where($where);
        }

        return $query->fetch()['count'];
    }
}