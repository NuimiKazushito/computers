<?php

namespace Classes\User;

use Classes\StaticFunctions;
use Classes\Super\Paginator;

class Facade
{
    private Manager $userManager;
    private Paginator $paginator;

    public function __construct(Paginator $paginator, Manager $userManager)
    {
        $this->userManager = $userManager;
        $this->paginator = $paginator;
    }

    public function getUserData(int $page, bool $html = false): array|string
    {

        $query = $this->userManager->getAllUsers(false, true);
        $this->paginator->setQueryLimit($query, $this->paginator->getOffset($page));

        $data = $query->fetchAll();

        if ($html)
        {
            return StaticFunctions::renderLatte('usersTable', [
                    'users' => $data,
                    'pages' =>  $this->paginator->getNumberOfPages(),
                    'page' =>  $page,
                    'ajax' => 'userTable'
                ]
            );
        } else {
            return [
                'pages' =>  $this->paginator->getNumberOfPages(),
                'data' => $data
            ];
        }
    }
}