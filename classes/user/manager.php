<?php

namespace Classes\User;

use Classes\Login\Manager as LManager;
use Classes\UserData;

class Manager extends \Classes\Dibi
{
    const TABLE = 'tUser';
    const PRIMARY = 'id';

    const EMAIL = 'email';
    const FIRST_NAME = 'firstName';
    const SECOND_NAME = 'secondName';
    const PHONE = 'phone';

    public function getAllUsers(bool $notMe = false, bool $onlyQuery = false): \Dibi\Fluent|array
    {
        $query = $this->db->select(self::FIRST_NAME)
            ->select(self::SECOND_NAME)
            ->select(self::EMAIL)
            ->select('u.%n', self::PRIMARY)
            ->select(LManager::USER_NAME)
            ->select(LManager::PERMISSION);
        $query->from(self::TABLE)->as('u');
        $query->leftJoin(LManager::TABLE)->as('l')->on('u.%n = l.%n',
            self::PRIMARY, LManager::FK_USER_ID);
        $query->where('%n IS NOT NULL', LManager::USER_NAME);

        if ($notMe)
        {
            $query->where('u.%n != %i',
                self::PRIMARY, UserData::getUserID());
        }

        if ($onlyQuery)
        {
            return $query;
        } else {
            return $query->fetchAll();
        }
    }

    public function getByEmail(mixed $email): array|null
    {
        $query = $this->db->select(self::ALL)
            ->select('l.%n', LManager::PRIMARY)->as('loginID');
        $query->from(self::TABLE)->as('u')
            ->join(LManager::TABLE)->as('l')->on('u.%n = l.%n',
                self::PRIMARY, LManager::FK_USER_ID);
        $query->where('LOWER(u.%n) = %s', self::EMAIL, $email);

        return (array) $query->fetch();
    }
}