<?php

namespace Classes\ServerUsers;

use Classes\Computer\Manager as CManager;
use Classes\CurrentPcPassword\Manager as CPManager;
use Classes\Dibi;
use Dibi\Exception;

class Manager extends Dibi
{
    const TABLE = 'tServerUsers';
    const PRIMARY = 'id';

    const FK_COMPUTER_ID = 'FK_computerID';
    const USER_NAME = 'userName';
    const PASSWORD = 'password';

    public function getAllUsers(): array
    {
        $query = $this->db->select(self::USER_NAME)
            ->select(self::PASSWORD)
            ->select('cp.%n', CPManager::CURRENT_PASSWORD)
            ->select('cp.%n', CPManager::PRIMARY)->as('passwordID')
            ->select(CManager::NAME)
            ->select('c.%n', CManager::PRIMARY)->as('computer')
            ->select('c.%n', CManager::STATE)
            ->select('c.%n', CManager::STATION)
            ->select('su.%n', self::PRIMARY)->as('user')
            ->select(CPManager::END_DT)
            ->select('su.%n', self::PRIMARY)->as('serverUserID')
            ->select('cp.%n', CPManager::PRIMARY)->as('currentPasswordID')
            ->select('NOW()')->as('now');
        $query->from(self::TABLE)->as('su');
        $query->leftJoin(CManager::TABLE)->as('c')->on('su.%n = c.%n',
                self::FK_COMPUTER_ID, CManager::PRIMARY)
            ->leftJoin(CPManager::TABLE)->as('cp')->on('su.%n = cp.%n',
                self::FK_COMPUTER_ID, CPManager::PC_ID);
        $query->orderBy('c.%n', CManager::PRIMARY);

        return $query->fetchAll();
    }

    public function findUserByComputerID(int $pc): array|null
    {
        return (array) $this->db->select(self::USER_NAME)
            ->from(self::TABLE)
            ->where('%n = %i', self::FK_COMPUTER_ID, $pc)
            ->fetch();
    }

    public function updateAllPasswords(string $password): bool
    {
        try {
            $this->db->update(self::TABLE, [
                    self::PASSWORD => $password
                ]
            )->execute();
            return true;
        } catch (Exception $e) {
            bdump($e);
            return false;
        }
    }
}