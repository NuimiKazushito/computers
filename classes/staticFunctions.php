<?php
namespace Classes;

use Classes\Login\Manager as LManager;
use Classes\Reservation\Filter;

class StaticFunctions
{
    public static function crypt($first, $second) : string
    {
        return hash_hmac("SHA512", $first, $second);
    }

    public static function passwordEncrypt(int $length = 50) : string
    {
        $alphabet = '123456789abcdefghijkmnpqrstuvwx123456789ABCDEFGHJKLMNPQRSTUVWX123456789';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++)
        {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public static function addSuccessMessage(string $message) : void
    {
        $array['message'] = $message;
        $array['severity'] = DefaultClass::ALERT_SUCCESS;
        $array['title'] = 'Skvěle';
        $_SESSION['alert'][] = $array;
    }

    public static function addErrorMessage(string $message) : void
    {
        $array['message'] = $message;
        $array['severity'] = DefaultClass::ALERT_ERROR;
        $array['title'] = 'Sakryš!!';
        $_SESSION['alert'][] = $array;
    }

    public static function renderLatte(string $file, array $data, string $specific = 'www/admin/tables') : string
    {
        $latte = new \Latte\Engine();
        return $latte->renderToString(sprintf('%s/%s.latte', $specific, $file), $data);
    }

    public static function setHeader(string $location) : void
    {
        $url = StaticFunctions::getCurrentURL() .$location;
        header('Location: '. $url);
        exit;
    }

    public static function getCurrentURL(): string
    {
        preg_match('~^([^?]+)~', $_SERVER['REQUEST_URI'], $url);
        $urls = explode('/', $url[1]);
        if ($urls[1] == 'esport')
        {
            return '/esport';
        } else {
            return '';
        }
    }

    public static function isMobile(): bool|int
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    public static function setFilter(string $from, string $to, string $email): void
    {
        $_SESSION['filter'] = new Filter($from ? new DateTimeUtil($from) : null, $to ? new DateTimeUtil($to) : null, $email ?? null);
    }

    public static function getFilter() : ?\Classes\Reservation\Filter
    {
        return $_SESSION['filter'] ?? null;
    }

    public static function emptyFilter() : void
    {
        unset($_SESSION['filter']);
    }

    public static function isUnderMaintenance() : bool
    {
        return (file_get_contents('./scripts/maintenance.txt') == "1");
    }

    public static function changeMaintenanceState() : void
    {
        $actualState = self::isUnderMaintenance();
        file_put_contents('./scripts/maintenance.txt', (string)!$actualState);
    }

}