<?php

namespace Classes\Games;

use Classes\Dibi;

class Manager extends Dibi
{
    const TABLE = 'tGames';
    const PRIMARY = 'id';

    const NAME = 'name';
    const CLIENT = 'client';

    public function getAllClients(): array
    {
        $query = $this->db->select(self::CLIENT);
        $query->from(self::TABLE);
        $query->groupBy(self::CLIENT);

        return $query->fetchAll();
    }
}