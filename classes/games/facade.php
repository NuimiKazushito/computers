<?php
namespace Classes\Games;

use Classes\Games\Manager as GManager;
use Classes\StaticFunctions as SF;

class Facade
{
    private GManager $gameManager;

    public function __construct(GManager $gameManager)
    {
        $this->gameManager = $gameManager;
    }


    public function getALlInformation($html = false)
    {
        $data = $this->gameManager->getAll();

        foreach ($data as $id => $game)
        {
            $json[GManager::NAME] = $game[GManager::NAME];
            $json[GManager::CLIENT] = $game[GManager::CLIENT];

            $data[$id]['json'] = json_encode($json);
        }

        if ($html)
        {
            return SF::renderLatte('games', [
                    'allGames' => $data
                ]
            );

        } else {
            return $data;
        }
    }

    public function getAllGames()
    {
        $data = $this->gameManager->getAll();

        $return = [];
        foreach ($data as $game)
        {
            $return[$game[GManager::CLIENT]][] = $game[GManager::NAME];
        }

        return $return;
    }
}