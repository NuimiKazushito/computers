<?php

namespace Classes\UHKEmails;

use Classes\Dibi;

class Manager extends Dibi
{
    const TABLE = 'tUHKEmails';
    const PRIMARY = 'id';

    const LOGIN = 'login';
    const NORMAL_EMAIL = 'normalEmail';
    const SHORTENED_EMAIL = 'shortenedEmail';
}