<?php

namespace Classes\Rules;

use Classes\DateTimeUtil;
use Classes\Dibi;
use Classes\UserData;
use Dibi\Exception;

class Manager extends Dibi
{
    const TABLE = 'tRules';
    const PRIMARY = 'id';

    const HOURS = 'hours';
    const PER = 'per';
    const PC = 'numberOfPCs';
    const DESCRIPTION = 'description';

    const RU_TABLE = 'user_rules';
    const RU_PRIMARY = 'id';
    const RU_USER = 'FK_userID';
    const RU_RULE = 'FK_ruleID';
    const RU_DATE = 'date';
    const RU_COUNT = 'myCount';

    const PER_DICTIONARY = [
        1 => "Den",
        2 => "Týden",
        3 => "Měsíc",
        4 => "Rok"
    ];

    public function getRUbyID(int $id): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::RU_TABLE);
        $query->where('%n = %i', self::RU_PRIMARY, $id);

        return (array) $query->fetch();
    }

    public function myActiveRule(DateTimeUtil $from, DateTimeUtil $to): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::RU_TABLE);
        $query->where('%n = %i', self::RU_USER, UserData::getUserID())
            ->where("(%n >= %d AND %n <= %d)",
                self::RU_DATE, $from,
                self::RU_DATE, $to
            );

        return (array) $query->fetch();
    }

    public function usersActiveRule(DateTimeUtil $from, DateTimeUtil $to, int $userID): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::RU_TABLE);
        $query->where('%n = %i', self::RU_USER, $userID)
            ->where("(%n >= %d AND %n <= %d)",
                self::RU_DATE, $from,
                self::RU_DATE, $to
            );

        return (array) $query->fetch();
    }

    public function getRUID(bool $main = true) : int
    {
        $query = $this->db->select(self::PRIMARY);
        $query->from(self::TABLE);

        if ($main)
        {
            $query->where('%n IS NOT NULL', self::HOURS)
                ->where('%n IS NOT NULL', self::PER);
        } else {
            $query->where('%n IS NOT NULL', self::PC);
        }
        return $query->fetch()[self::PRIMARY];
    }

    public function addRuleForUser(array $insert): void
    {
        try {
            $this->db->insert(self::RU_TABLE, $insert)->execute();
        } catch (Exception $e) {
            bdump($e);
        }
    }


    public function deleteUserRule(int $id): void
    {
        try {
            $this->db->delete(self::RU_TABLE)->where('%n = %i', self::RU_PRIMARY, $id)->execute();
        } catch (Exception $e) {
            bdump($e);
        }
    }

    public function updateRule(array $update, int $id): void
    {
        try {
            $this->db->update(self::RU_TABLE, $update)->where('%n = %i', self::RU_PRIMARY, $id)->execute();
        } catch (Exception $e) {
            bdump($e);
        }
    }

    public function getUserRuleData(bool $onlyQuery = false): \Dibi\Fluent|array
    {
        $query = $this->db->select('ru.%n', self::RU_COUNT)
            ->select('ru.%n', self::RU_DATE)
            ->select('ru.%n', self::PRIMARY)
            ->select('r.%n', self::HOURS)
            ->select('u.%n', \Classes\User\Manager::EMAIL)
            ->select('u.%n', \Classes\User\Manager::FIRST_NAME)
            ->select('u.%n', \Classes\User\Manager::SECOND_NAME);
        $query->from(self::RU_TABLE)->as('ru')
            ->leftJoin(self::TABLE)->as('r')->on('ru.%n = r.%n',
                self::RU_RULE, self::PRIMARY)
            ->leftJoin(\Classes\User\Manager::TABLE)->as('u')->on('ru.%n = u.%n',
                self::RU_USER, \Classes\User\Manager::PRIMARY);
        $now = new DateTimeUtil();
        $query->where('%n >= %d AND %n <= %d',
            self::RU_DATE, $now->getMyWeek()['start'],
            self::RU_DATE, $now->getMyWeek()['end']);
        $query->orderBy(self::RU_COUNT)->desc();

        if($onlyQuery)
        {
            return $query;
        } else {
            return $query->fetchAll();
        }
    }

}