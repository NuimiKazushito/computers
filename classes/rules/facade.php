<?php

namespace Classes\Rules;

use Classes\DateTimeUtil;
use Classes\StaticFunctions as SF;
use Classes\Super\Paginator;
use Classes\UserData;

class Facade
{
    private Manager $rulesManager;
    private Paginator $paginator;

    public function __construct(Manager $rulesManager, Paginator $paginator)
    {
        $this->rulesManager = $rulesManager;
        $this->paginator = $paginator;
    }

    public function checkTimeRule(DateTimeUtil $from, DateTimeUtil $to): array
    {
        if (UserData::isAdmin())
        {
            return [
                'leftHours' => 0,
                'allowed' => true
            ];
        }
        $userRule = $this->rulesManager->myActiveRule($from->getMyWeek()['start'], $from->getMyWeek()['end']);
        $timeRule = $this->rulesManager->getAllByPrimary($this->rulesManager->getRUID());
        $diff = $from->getHourDiff($to);


        if (empty($userRule))
        {
            //pravidlo neexistuje pro daný týden, vytvoří se s rezervací
            $allowed = true;
            $leftHours = $timeRule[Manager::HOURS] - $diff;
        } else {
            //pravidlo existuje je kontrola
            $count = $userRule[Manager::RU_COUNT] + $diff;
            if ($count <= $timeRule[Manager::HOURS])
            {
                //nepřekročil čas
                $allowed = true;
                $leftHours = $timeRule[Manager::HOURS] - $count;
            } else {
                //překročil čas čas
                $allowed = false;
                $leftHours = $timeRule[Manager::HOURS] - $userRule[Manager::RU_COUNT];
            }

        }

        return [
            'leftHours' => $leftHours,
            'allowed' => $allowed
        ];
    }

    public function manageRule(DateTimeUtil $from, DateTimeUtil $to, int $userID = 0, bool $minus = false): void
    {
        if ($userID == 0)
        {
            $userRule = $this->rulesManager->myActiveRule($from->getMyWeek()['start'], $from->getMyWeek()['end']);
        } else {
            $userRule = $this->rulesManager->usersActiveRule($from->getMyWeek()['start'], $from->getMyWeek()['end'], $userID);
        }
        $timeRule = $this->rulesManager->getAllByPrimary($this->rulesManager->getRUID());
        $diff = $from->getHourDiff($to);
        if (empty($userRule)) {
            $insert[Manager::RU_RULE] = $timeRule[Manager::PRIMARY];
            $insert[Manager::RU_USER] = UserData::getUserID();
            $insert[Manager::RU_DATE] = $from;
            $insert[Manager::RU_COUNT] = $diff;
            $this->rulesManager->addRuleForUser($insert);
        } else {
            if ($minus)
            {
                $update[Manager::RU_COUNT] = $userRule[Manager::RU_COUNT] - $diff;
            } else {
                $update[Manager::RU_COUNT] = $userRule[Manager::RU_COUNT] + $diff;
            }
            $this->rulesManager->updateRule($update, $userRule[Manager::RU_PRIMARY]);
        }
    }

    public function getUserRules(int $page, bool $html = false): array|string
    {
        $dataQuery = $this->rulesManager->getUserRuleData(true);
        $this->paginator->setQueryLimit($dataQuery, $this->paginator->getOffset($page));
        $rulesData = $dataQuery->fetchAll();

        foreach ($rulesData as $key => $data)
        {
            $date = new DateTimeUtil($data['date']);
            $rulesData[$key]['date'] = $date->getDateFormat();
        }

        if ($html)
        {
            return SF::renderLatte('rulesTable', [
                    'userRules' => $rulesData,
                    'pages' =>  $this->paginator->getNumberOfPages(),
                    'page' =>  $page,
                    'ajax' => 'rulesTable'
                ]
            );
        } else {
            return [
                'pages' =>  $this->paginator->getNumberOfPages(),
                'data' => $rulesData
            ];
        }
    }

}