<?php

namespace Classes\team;

use Classes\DateTimeUtil;

class Team
{
    private String $name;
    private int $id;
    private bool $isLeader;

    public function __construct(string $name, int $id, bool $isLeader)
    {
        $this->name = $name;
        $this->id = $id;
        $this->isLeader = $isLeader;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isLeader(): bool
    {
        return $this->isLeader;
    }
}