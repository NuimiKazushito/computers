<?php

namespace Classes\team;

use Classes\StaticFunctions;
use Classes\team\Manager as TManager;
use Classes\UserData;

class Facade
{
    private TManager $teamManager;

    public function __construct(TManager $teamManager)
    {
        $this->teamManager = $teamManager;
    }

    public function getMembers(bool $hml = false): array|string
    {
        $data = is_null(UserData::getTeam()) ? [] : $this->teamManager->getMyMembers();

        if ($hml)
        {
            return StaticFunctions::renderLatte('team', [
                    'members' => $data
                ]
            );
        } else {
            return $data;
        }

    }

    public function getMembersAdd(array $users, ?array $members, bool $html = false): string|array
    {
        $return = [];
        foreach ($users as $user)
        {
            if (!is_null($members))
            {
                $isMember = false;
                foreach ($members as $member)
                {
                    if ($user['email'] == $member['email'])
                    {
                        $isMember = true;
                    }
                }

                if(!$isMember)
                {
                    $return[] = $user;
                }
            }
        }

        if ($html)
        {
            return StaticFunctions::renderLatte('addMember',[
                    'usersAdd' => $return
                ],
                'www/admin/forms/'
            );
        }
        return $return;
    }

}