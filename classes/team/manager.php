<?php

namespace Classes\team;

use Classes\Dibi;
use Classes\UserData;
use Dibi\Exception;
use Classes\User\Manager as UManager;
use Classes\Login\Manager as LManager;

class Manager extends Dibi
{
    const TABLE = 'tTeam';
    const PRIMARY = 'id';

    const NAME = 'name';
    const USER = 'FK_userID';

    const UT_TABLE = 'user_team';
    const UT_PRIMARY = 'id';
    const UT_USER = 'FK_userID';
    const UT_TEAM = 'FK_teamID';

    public function getMyTeam(): ?array
    {
        $query = $this->db->select('t.%n',self::ALL);
        $query->from(self::TABLE)->as('t')
            ->leftJoin(self::UT_TABLE)->as('ut')->on('t.%n = ut.%n',
                self::PRIMARY, self::UT_TEAM);
        $query->where('ut.%n = %i',
            self::UT_USER, UserData::getUserID());

        return $query->fetch() ? $query->fetch()->toArray() : null;
    }

    public function addToTeam(int $teamID, mixed $memberID): void
    {
        try {
            $this->db->insert(self::UT_TABLE, [
                    self::UT_USER => $memberID,
                    self::UT_TEAM => $teamID
                ]
            )->execute();
        } catch (Exception $e) {
            bdump($e);
        }
    }

    public function getMyMembers(): array
    {
        $query = $this->db->select('u.%n', UManager::PRIMARY)
            ->select(UManager::FIRST_NAME)
            ->select(UManager::SECOND_NAME)
            ->select(UManager::EMAIL)
            ->select(LManager::USER_NAME);
        $query->from(self::UT_TABLE)->as('ut')
            ->leftJoin(UManager::TABLE)->as('u')->on('ut.%n = u.%n',
                self::UT_USER, UManager::PRIMARY)
            ->leftJoin(LManager::TABLE)->as('l')->on('u.%n = l.%n',
                UManager::PRIMARY, LManager::FK_USER_ID);
        $query->where('ut.%n = %i', self::UT_TEAM, UserData::getTeam()->getId());

        return $query->fetchAll();
    }

    public function deleteFromTeam(int $memberID): void
    {
        try {
            $this->db->delete(self::UT_TABLE)
                ->where('%n = %i', self::UT_TEAM, UserData::getTeam()->getId())
                ->where('%n = %i', self::UT_USER, $memberID)
                ->execute();
        } catch (Exception $e) {
            bdump($e);
        }
    }
}