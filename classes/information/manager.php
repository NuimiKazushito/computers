<?php

namespace Classes\Information;

use Classes\Dibi;

class Manager extends Dibi
{
    const TABLE = 'tInformation';
    const PRIMARY = 'id';

    const HEADER = 'header';
    const TYPE = 'type';
    const TEXT = 'text';
    const URL = 'url';

}