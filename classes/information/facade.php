<?php

namespace Classes\Information;

use Classes\Information\Manager as IManager;
use Classes\StaticFunctions as SF;

class Facade
{
    private IMAnager $informationManager;

    public function __construct(IMAnager $informationManager)
    {
        $this->informationManager = $informationManager;
    }

    public function getALlInformation($html = false)
    {
        $data = $this->informationManager->getAll();

        foreach ($data as $id => $information)
        {
            $json[IManager::TYPE] = $information[IManager::TYPE];
            $json[IManager::HEADER] = $information[IManager::HEADER];
            $json[IManager::TEXT] = $information[IManager::TEXT];
            $json[IManager::URL] = $information[IManager::URL];

            $data[$id]['json'] = json_encode($json);
        }

        if ($html)
        {
            return SF::renderLatte('information', [
                    'allInformation' => $data
                ]
            );

        } else {
            return $data;
        }
    }
}