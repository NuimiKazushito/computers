<?php

namespace Classes\ReservationAll;

use Classes\DateTimeUtil;
use Classes\ReservationAll\Manager as RAManager;
use Classes\Reservation\Manager as RManager;
use Classes\User\Manager as UManager;

class Facade
{
    private RAManager $reservationAllManager;
    private RManager $reservationManager;
    private UManager $userManager;

    public function __construct(RAManager $reservationAllManager, RManager $reservationManager, UManager $userManager)
    {
        $this->reservationAllManager = $reservationAllManager;
        $this->reservationManager = $reservationManager;
        $this->userManager = $userManager;
    }


    public function deleteReservations($id): array
    {
        $data = $this->reservationAllManager->getAllByPrimary($id);
        $userData = $this->userManager->getAllByPrimary($data['FK_userID']);
        $reservationData = $this->reservationManager->getAllByUserID($data['FK_userID']);

        foreach ($reservationData as $reservation)
        {
            $this->reservationManager->deleteByPrimary($reservation['id']);
        }

        $this->reservationAllManager->deleteByPrimary($id);

        return [
            'email' => $userData['email'],
            'from' => $data['timeFrom'],
            'to' => $data['timeTo'],
        ];
    }

    public function acceptReservation(int $reservationID, string $hashKey): array
    {
        $reservationAskData = $this->reservationAllManager->getAllByPrimary($reservationID);
        $reservationData = $this->reservationManager->getAllByUserID($reservationAskData['FK_userID']);
        $userData = $this->userManager->getAllByPrimary($reservationAskData['FK_userID']);

        foreach ($reservationData as $reservation)
        {
            $this->reservationManager->managerUpdate(['hashKey'=> $hashKey], $reservation['id']);
        }
        $this->reservationAllManager->managerUpdate(['confirmed' => 1, 'hashKey' => $hashKey], $reservationID);

        return[
            'email' => $userData['email'],
            'from' => $reservationAskData['timeFrom'],
            'to' => $reservationAskData['timeTo'],
            'hashKey' => $hashKey
        ];
    }

    public function dennyReservation(int $id): array
    {
        $reservationAskData = $this->reservationAllManager->getAllByPrimary($id);
        $reservationData = $this->reservationManager->getAllByUserID($reservationAskData['FK_userID']);
        $userData = $this->userManager->getAllByPrimary($reservationAskData['FK_userID']);

        foreach ($reservationData as $reservation)
        {
            $this->reservationManager->deleteByPrimary($reservation['id']);
        }
        $this->reservationAllManager->deleteByPrimary($id);
        $this->userManager->deleteByPrimary($reservationAskData['FK_userID']);

        return [
            'email' => $userData['email'],
            'from' => $reservationAskData['timeFrom'],
            'to' => $reservationAskData['timeTo'],
        ];
    }
}