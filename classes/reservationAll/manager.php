<?php

namespace Classes\ReservationAll;

use Classes\Login\Manager as LManager;
use Classes\ReservationAll\Manager as RSManager;
use Classes\StaticFunctions;
use Classes\User\Manager as UManager;
use Classes\Computer\Manager as CManager;

class Manager extends \Classes\Dibi
{
    const TABLE = 'tReservationAll';
    const PRIMARY = 'id';

    const FK_USER_ID = 'FK_userID';
    const CREATE_DT = 'createDT';
    const TIME_FROM = 'timeFrom';
    const TIME_TO = 'timeTo';
    const HASH_KEY = 'hashKey';
    const CONFIRMED = 'confirmed';

    const TO_PROCESS = 'TO_PROCESS';

    const C_TO_PROCESS = 0;
    const C_PROCESSED = 1;
    const C_DENIED = 2;
    const CONFIRM_DICTIONARY = [
        self::C_TO_PROCESS => 'Potřeba vyřešit',
        self::C_PROCESSED => 'Zpracováno úspěšně',
        self::C_DENIED => 'Zamítnuto'
    ];

    public function getAllReservations()
    {
        $query = $this->db->select(self::TIME_FROM);
        $query->from(self::TABLE)->as('r');
        $query->select(self::TIME_TO)
            ->select(self::HASH_KEY)
            ->select(self::CONFIRMED)
            ->select(UManager::FIRST_NAME)
            ->select(UManager::SECOND_NAME)
            ->select(UManager::EMAIL)
            ->select(UManager::PHONE)
            ->select('%s', 'Celá učebna')->as(CManager::NAME)
            ->select('r.%n', self::PRIMARY)->as('reservationID');
        $query->leftJoin(UManager::TABLE)->as('u')->on('r.%n = u.%n',
            self::FK_USER_ID, UManager::PRIMARY);

        $query->where('%n >= %d', self::TIME_FROM, new \DateTime());

        return $query->fetchAll();
    }
}