<?php
namespace Classes\Computer;

class Manager extends \Classes\Dibi
{
    const TABLE = 'tComputer';
    const PRIMARY = 'id';

    const NAME = 'computerName';
    const STATION = 'stationID';
    const BY_PERMISSION = 'byPermission';
    const STATE = 'state';

    public function getAllComputers(bool $all = false): array
    {
        $query = $this->db->select(self::ALL);
        $query->from(self::TABLE);

        if (! $all)
        {
            $query->where('%n < 11', self::PRIMARY);
        }
        return $query->fetchAll();
    }
}