<?php
    function moduleLoader($urls = array(), $container)
    {
        $tmp = explode('-', $urls[1]);
        $class = implode('\\', array_map('ucfirst', $tmp)) . (count($tmp) > 1 ? $container::MODULE : '');
        $m1 = new ReflectionMethod ($class, 'getInstance');
        $o = $m1->invoke(null, $container);
        $m2 = new ReflectionMethod ($class, 'init');
        $m2->invoke($o, $urls);
    }

    function formatString(array $stringList)
    {
        $string = reset($stringList);
        if (count($stringList) > 1 && preg_match('/%(d|s)/u', $string))
        {
            $string = call_user_func_array('sprintf', $stringList);
        }
        return $string;
    }
