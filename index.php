<?php
use Tracy\Debugger;
require_once 'common_functions.php';

$whitelist = [
    '127.0.0.1',
    '::1'
];

require_once 'autoload.php';

session_start();

Debugger::enable('localhost');


try {
    require_once 'config/config.php';
    Debugger::$showBar = LOCALE;

    dibi::connect([
        'driver' => DB_DRIVER,
        'host' => DB_HOST,
        'username' => DB_USERNAME,
        'password' => DB_PASSWORD,
        'database' => DB_DATABASE,
        'charset' => DB_CHARSET,
    ]);
    $panel = new \Dibi\Bridges\Tracy\Panel();
    $panel->register(dibi::getConnection());
} catch (\Dibi\Exception $e) {
}

$container = new \Presenters\Container();

$REQUEST_URI = &$_SERVER['REQUEST_URI'];
if ($REQUEST_URI == null)
{
    $argv = &$argv;
    $REQUEST_URI = $argv[1];
}

preg_match('~^([^?]+)~', $REQUEST_URI, $url);
$urls = explode('/', $url[1]);

if (!isset($urls[1]) || !strlen($urls[1]))
{
    $urls[1] = "Home";
}

try {
    moduleLoader($urls, $container);
} catch (Exception $e)
{
echo $e;
}
