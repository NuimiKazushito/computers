<?php

use Classes\Computer\Manager as CManager;
use Classes\CronLog\CronLogger;
use Classes\CurrentPcPassword\Manager as CPPManager;
use Classes\Reservation\Manager as RManager;

class Cron extends \Classes\DefaultClass
{
    public function renderCheckPasswordChange()
    {
        $logger = new CronLogger($this->container->getCronLogManager());
        $currentReservation = $this->container->getReservationManager()->getAllToCheck();

        if (count($currentReservation) > 0)
        {
            foreach($currentReservation as $current)
            {
                $this->container->getReservationFacade()->changePasswordPS($current['userName'], $current['hashKey']);

                $this->container->getCurrentPcPasswordManager()
                    ->updateByComputerID($current['computer'], $current[RManager::HASH_KEY], $current[RManager::TIME_TO], $current['reservation']);
                $logger->add($current['computer']);
            }
        } else {
            bdump('nothing found');
        }
        $logger->end();
    }

    public function renderCheckEndOfTime()
    {
//        $logger = new CronLogger($this->container->getCronLogManager());
        $endOfReservation = $this->container->getReservationManager()->getAllToCheck(true);

        if (count($endOfReservation) > 0)
        {
            foreach($endOfReservation as $end)
            {
                dumpe($end);
            }
        } else {
            bdump('nothing found');
        }
//        $logger->end();
    }

    public function renderChangePasswords()
    {
        $logger = new CronLogger($this->container->getCronLogManager());
        $allPasswords = $this->container->getServerUsersManager()->getAllUsers();
        foreach ($allPasswords as $computer)
        {
            $password = $this->container->getReservationFacade()->getHashKey();
            $this->container->getReservationFacade()->changePasswordPS($computer['userName'], $password);
            $this->container->getServerUsersManager()->managerUpdate(['password' => $password], $computer['serverUserID']);
            $logger->add($computer['computer']);
            if (!is_null($computer['currentPasswordID']))
            {
                $this->container->getCurrentPcPasswordManager()->managerUpdate(['currentPassword' => $password], $computer['currentPasswordID']);
            }
        }
        $logger->end();
    }
}