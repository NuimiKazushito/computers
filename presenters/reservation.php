<?php

use Classes\Reservation\Manager as RManager;
use Classes\ReservationAll\Manager as RAManager;
use Classes\Login\Manager as LManager;
use Classes\User\Manager as UManager;
use Classes\Computer\Manager as CManager;
use Classes\StaticFunctions as SF;
use Classes\DateTimeUtil;
use Classes\UserData;


class Reservation extends \Classes\DefaultClass
{
    public function renderCheckExisted(): void
    {
        $this->addData('allComputers', $this->container->getComputerManager()->getAllComputers());
        if ($_POST)
        {
            $hashKey = $this->getParameter('hashKey');
            if ($this->container->getReservationManager()->doesKeyExist($hashKey))
            {
                if($this->container->getReservationFacade()->useKey($hashKey, $this->getParameter('computerID')))
                {
                    $this->addData('messageKey', [
                            'success' => 'Úspěšné použití klíče'
                        ]
                    );
                    $this->addData('killMe', true);
                } else {
                    $this->addData('messageKey', [
                            'danger' => 'Tento klíč nelze použít pro tento počítač'
                        ]
                    );
                }
            } else {
                $this->addData('messageKey', [
                        'danger' => 'Tento klíč neexistuje'
                    ]
                );
            }
        }

        $this->renderLayout('check');
    }


    public function renderCreateReservation(): void
    {
        $computerID = $this->getParameter('computerID');

        $userData = $this->container->getUserManager()->getAllByPrimary(UserData::getUserID());
        $dateTimeFrom = new DateTimeUtil($this->getParameter('timeFrom'));
        $dateTimeTo = new DateTimeUtil($this->getParameter('timeTo'));

        if ($dateTimeFrom->getCompareFormat() == $dateTimeTo->getCompareFormat())
        {
            SF::addErrorMessage('Prosím vyplňte datum od do');
            SF::setHeader('/admin');
        }

        $reservations = $this->container->getReservationManager()
            ->findAllByComputerAndDate($computerID, $dateTimeFrom, $dateTimeTo);

        if (count($reservations) > 0)
        {
            SF::addErrorMessage('Omlouváme se, ale v tomto termínu již rezervace existuje');
            SF::setHeader('/admin');
        }

        //whole esport reservation
        if ($computerID == 11) {
            $hashKey = $this->container->getReservationFacade()->getHashKey();
            for ($i = 1; $i < 11; $i++) {
                $dataReservation = [];
                $dataReservation[RManager::FK_USER_ID] = UserData::getUserID();
                $dataReservation[RManager::CREATE_DT] = new DateTimeUtil();
                $dataReservation[RManager::HASH_KEY] = $hashKey;
                $dataReservation[RManager::ONE_TIME] = 1;

                $dataReservation[RManager::FK_COMPUTER_ID] = $i;
                $dataReservation[RManager::TIME_FROM] = $dateTimeFrom;
                $dataReservation[RManager::TIME_TO] = $dateTimeTo;

                $this->container->getReservationManager()->managerInsert($dataReservation);
            }

            $dataReservationAll = [];
            $dataReservationAll[RAManager::FK_USER_ID] = UserData::getUserID();
            $dataReservationAll[RAManager::CREATE_DT] = new DateTimeUtil();
            $dataReservationAll[RAManager::TIME_FROM] = $dateTimeFrom;
            $dataReservationAll[RAManager::TIME_TO] = $dateTimeTo;
            $dataReservationAll[RAManager::HASH_KEY] = $hashKey;
            $resID = $this->container->getReservationAllManager()->managerInsert($dataReservationAll, true);

            for ($i = 1; $i < 11; $i++) {
                $this->container->getReservationFacade()
                    ->checkPassword($resID, $dateTimeFrom, $dateTimeTo, $hashKey, $i, 'hrac' . $i);
            }

            $this->container->getMail()->sendRegistrationMessage(
                $userData['email'],
                $hashKey,
                '1 - 10',
                'hrac1 - hrac10',
                $dateTimeFrom,
                $dateTimeTo
            );


            SF::addSuccessMessage(sprintf('Registrace byla úspěšná. Váš kód je %s', $hashKey));
        } else {
            $dataReservation = [];
            $dataReservation[RManager::FK_USER_ID] = UserData::getUserID();
            $dataReservation[RManager::CREATE_DT] = new DateTimeUtil();
            $dataReservation[RManager::HASH_KEY] = $this->container->getReservationFacade()->getHashKey();
            $dataReservation[RManager::ONE_TIME] = 1;

            $dataReservation[RManager::FK_COMPUTER_ID] = $computerID;
            $dataReservation[RManager::TIME_FROM] = $dateTimeFrom;
            $dataReservation[RManager::TIME_TO] = $dateTimeTo;

            if (UserData::isUser()) {
                $this->container->getRulesFacade()->manageRule(new DateTimeUtil($dateTimeFrom),new DateTimeUtil($dateTimeTo));
            }

            $resID = $this->container->getReservationManager()->managerInsert($dataReservation, true);

            $userName = $this->container->getServerUsersManager()->findUserByComputerID($computerID)['userName'];

            $this->container->getReservationFacade()
                ->checkPassword($resID, $dateTimeFrom, $dateTimeTo, $dataReservation[RManager::HASH_KEY], $computerID, $userName);

            $this->container->getMail()->sendRegistrationMessage(
                $userData['email'],
                $dataReservation[RManager::HASH_KEY],
                $computerID,
                $userName,
                $dateTimeFrom,
                $dateTimeTo,
                $resID
            );
            SF::addSuccessMessage(sprintf('Registrace byla úspěšná. Váš kód je %s', $dataReservation[RManager::HASH_KEY]));
        }

        SF::setHeader('/admin');
    }

    public function renderDeleteOld()
    {
        $used = $this->getParameter('used');
        if ($this->container->getReservationManager()->deleteOldNadUsed($used === 'true'))
        {
            SF::addSuccessMessage('Rezervace byly smazány');
        } else {
            SF::addErrorMessage('Něco se nepodařilo, kontaktujte správce');
        }

        SF::setHeader('/admin');
    }

    public function renderDeleteMyReservation()
    {
        $this->container->getReservationManager()->deleteByPrimary($this->getParameter('id'));
        SF::addSuccessMessage("Rezervace byla smazána");
        SF::setHeader('/');
    }
}