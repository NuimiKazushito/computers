<?php

use \Classes\Login\Manager as LManager;
use Classes\StaticFunctions as SF;
use \Classes\User\Manager as UManager;
use Classes\UserData;

class User extends \Classes\DefaultClass
{
    public function renderDefault()
    {
        $this->setActiveMenu(self::M_USER);
        $this->setActiveModal(self::MODAL_USER);
        $this->isLoggedIn();

        if ($_POST)
        {
            $dataEdit[LManager::PASSWORD_ENCRYPT] = SF::passwordEncrypt();
            $dataEdit[LManager::PASSWORD] = SF::crypt($this->getParameter('password'), $dataEdit[LManager::PASSWORD_ENCRYPT]);
            $loginData = $this->container->getLoginManager()->getAllByUserId($this->getParameter('userID'));
            $this->container->getLoginManager()->managerUpdate($dataEdit, $loginData['id']);
            SF::addSuccessMessage('Heslo bylo změněno');
        }

        $page = 1;
        $users = $this->container->getUserFacade()->getUserData($page);
        $this->addData('users', $users['data']);
        $this->addData('pages', $users['pages']);
        $this->addData('page', $page);
        $this->addData('ajax', 'userTable');

        $this->renderLayout('index');
    }

    public function renderAddUser()
    {
        $this->setActiveMenu(self::M_USER);
        $this->isLoggedIn();

        $this->renderLayout('index');
    }


    public function renderEditMe()
    {
        $this->setActiveMenu(self::M_USER);
        $this->isLoggedIn(false);

        $this->renderLayout('editMe');
    }

    public function renderForgottenPassword()
    {
        $data = $this->getParameter();

        $this->addData("newPassword", false);
        $layout = "password";
        if (array_key_exists("email", $data))
        {
            $this->addData("newPassword", true);
            $this->addData("email", $data['email']);
            $layout = "passwordNew";
        }

        $this->renderLayout($layout, "home");
    }

    public function renderRenewPassword()
    {
        $data = $this->getParameter();

        if (array_key_exists("password", $data))
        {
            $data['email'] = array_key_exists('email', $data) ? base64_decode($data['email']) : UserData::getEmail();
            $passwordEncode = SF::passwordEncrypt();
            $password = SF::crypt($data['password'], $passwordEncode);
            $user = $this->container->getUserManager()->getByEmail($data['email']);
            $this->container->getLoginManager()->managerUpdate([
                    LManager::PASSWORD => $password,
                    LManager::PASSWORD_ENCRYPT => $passwordEncode
                ], $user['loginID']
            );
            SF::addSuccessMessage("Vaše heslo bylo změněno na to které jste zadal/a");
            if(UserData::checkLoggedIn())
            {
                SF::setHeader("/user/editMe");
            } else {
                SF::setHeader("/home/login");
            }
        } else {
            $this->container->getMail()->passwordRenewal($data['email']);
            SF::addSuccessMessage("Email pro obnovu hesla byl zaslán");
            SF::setHeader("/user/forgottenPassword");
        }

    }

    public function renderLogin()
    {
        $login = $this->getParameter('login');
        $pass = $this->getParameter('password');
        $allData = $this->container->getLoginManager()->getAllByLogin($login);
        if ($allData)
        {
            if (!array_key_exists($allData[UManager::EMAIL], array_change_key_case($this->container->getBlackListManager()
                ->getAll('email', ['state' => \Classes\BlackList\Manager::NOT_BLACKLISTED])))
            )
            {
                $encrypt = SF::crypt($pass, $allData[LManager::PASSWORD_ENCRYPT]);
                if ($encrypt === $allData[LManager::PASSWORD])
                {
                    new \Classes\UserData($allData[UManager::EMAIL], $allData[LManager::FK_USER_ID],
                        $allData[LManager::PERMISSION], $allData[LManager::USER_NAME]);

                    $team = $this->container->getTeamManager()->getMyTeam();
                    if(! is_null($team))
                    {
                        UserData::setTeam($team['name'], $team['id'], $team['FK_userID'] == UserData::getUserID());
                    }

                    SF::addSuccessMessage('Byl jste úspěšně přihlášen/a');
                    SF::setHeader( '/admin');
                } else {
                    SF::addErrorMessage('Špatné heslo');
                }
            } else {
                SF::addErrorMessage('Přístup do rezervačního systému Vám byl zakázán');
            }
        } else {
            SF::addErrorMessage('Špatný login');
        }

        SF::setHeader( '/home/login');
    }

    public function renderLogout()
    {
        session_unset();
        session_destroy();
        unset($_SESSION);

        session_start();

        $data = $this->getParameter();
        if (array_key_exists('sorry', $data))
        {
            SF::addSuccessMessage('Vypršela session');
        }

        if (array_key_exists('error', $data))
        {
            switch ($data['error'])
            {
                case 1:
                    SF::addErrorMessage('Nacházíte se na černé listině');
                break;
            }
        }

        SF::addSuccessMessage('Byl jste odhlášen/a');

        SF::setHeader( '/');
    }

    public function renderDisbandTeam()
    {
        $members = $this->container->getTeamFacade()->getMembers();
        foreach ($members as $member)
        {
            $this->container->getTeamManager()->deleteFromTeam($member['id']);
        }

        $this->container->getTeamManager()->deleteByPrimary(UserData::getTeam()->getId());
        UserData::disbandTeam();

        SF::addSuccessMessage("Tvůj tým byl úspěšně rozpuštěn");
        SF::setHeader("/admin/team");
    }

    public function renderRegister()
    {
        $dataUser = $dataLogin = [];

        $dataUser[UManager::EMAIL] = $this->getParameter('email');
        $dataUser[UManager::FIRST_NAME] = $this->getParameter('firstName');
        $dataUser[UManager::SECOND_NAME] = $this->getParameter('secondName');
        $dataUser[UManager::PHONE] = $this->getParameter('phone');

        if(empty($dataUser[UManager::EMAIL]) || empty($dataUser[UManager::FIRST_NAME]) || empty($dataUser[UManager::SECOND_NAME]) || empty($dataUser[UManager::PHONE]))
        {
            SF::addErrorMessage('Všechna pole musí být vyplněna');
            SF::setHeader(sprintf('/home/register?%s', http_build_query($this->getParameter())));
        }

        if ($this->container->getUserManager()->getByEmail($dataUser[UManager::EMAIL]))
        {
            SF::addErrorMessage('Tento email je již registrován');
            SF::setHeader(sprintf('/home/register?%s', http_build_query($this->getParameter())));
        }

        if (!$this->container->getConverter()->parseTextDocument($dataUser[UManager::EMAIL]))
        {
            SF::addErrorMessage('Zadejte validní FIM UHK email');
            SF::setHeader(sprintf('/home/register?%s', http_build_query($this->getParameter())));
        }

        $userID = $this->container->getUserManager()->managerInsert($dataUser, true);
        $dataLogin[LManager::USER_NAME] = $this->getParameter('userName');
        $dataLogin[LManager::FK_USER_ID] = $userID;
        $dataLogin[LManager::PERMISSION] = LManager::USER;
        $dataLogin[LManager::PASSWORD_ENCRYPT] = SF::passwordEncrypt();
        $dataLogin[LManager::PASSWORD] = SF::crypt($this->getParameter('password'), $dataLogin[LManager::PASSWORD_ENCRYPT]);
        SF::addSuccessMessage(sprintf('Registrace byla úspěšná. Nyní se můžete příhlásit pod jménem %s nebo emailem %s',
            $this->getParameter('userName'), $this->getParameter('email')));
        $this->container->getLoginManager()->managerInsert($dataLogin);

        SF::setHeader('/');
    }
}
