<?php

use Classes\BlackList\Manager as BManager;
use Classes\DateTimeUtil;
use Classes\DefaultClass;
use Classes\StaticFunctions as SF;
use Classes\UserData;

class Ajax extends DefaultClass
{

    private function sendJSON($data)
    {
        echo json_encode($data);
    }

    public function renderGeneratePassword(): void
    {
        $this->sendJSON(['password' => SF::passwordEncrypt(11)]);
    }

    public function renderGenerateRegistrationCalendar(): void
    {
        $data = $this->getParameter();
        $this->sendJSON($this->container->getReservationFacade()
            ->getAllReservationsToCalendar($data['computerID'], array_key_exists('pc', $data)));
    }

    public function renderChangeReservationData(): string
    {
        $search = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        return $this->container->getReservationFacade()->getAllOneTimeReservations($search, $this->getParameter('type'), true);
    }

    public function renderDeleteReservationData(): string
    {
        $resID = $this->getParameter('id');
        $type = $this->getParameter('type');

        if ($type)
        {
            $data = $this->container->getReservationAllFacade()->deleteReservations($resID);
        } else {
            $resData = $this->container->getReservationManager()->getAllByPrimary($resID);
            $data['email'] = $this->container->getUserManager()->getAllByPrimary($resData['FK_userID'])['email'];
            $data['from'] = $resData['timeFrom'];
            $data['to'] = $resData['timeTo'];
            $this->container->getRulesFacade()->manageRule(new DateTimeUtil($data['from']), new DateTimeUtil($data['to']),$resData['FK_userID'], true);
            $this->container->getReservationManager()->deleteByPrimary($resID);

        }

        $this->container->getMail()->sendDeletedReservation($data['email'], $data['from'], $data['to']);

        $search = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        return $this->container->getReservationFacade()->getAllOneTimeReservations($search, $type, true);
    }

    public function renderDeleteInformation(): string
    {
        $this->container->getInformationManager()->deleteByPrimary($this->getParameter('id'));

        return $this->container->getInformationFacade()->getALlInformation(true);
    }

    public function renderDeleteGame(): string
    {
        $this->container->getGamesManager()->deleteByPrimary($this->getParameter('deleteGame'));

        return $this->container->getGamesFacade()->getALlInformation(true);
    }

    public function renderAcceptReservation(): string
    {
        $data = $this->container->getReservationAllFacade()->acceptReservation($this->getParameter('reservationID'), $this->container->getReservationFacade()->getHashKey());
        $this->container->getMail()->sendAcceptReservationFinal($data['email'], $data['from'], $data['to'], $data['hashKey']);

        $search = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        return $this->container->getReservationFacade()->getAllOneTimeReservations($search, 1, true);
    }

    public function renderDennyReservation(): string
    {
        $data = $this->container->getReservationAllFacade()
            ->dennyReservation($this->getParameter('id'));

        if ($this->getParameter('email') == 'true')
        {
            $this->container->getMail()->sendDennyReservationFinal($data['email'], $data['from'], $data['to']);
        }

        $search = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        return $this->container->getReservationFacade()->getAllOneTimeReservations($search, 1, true);
    }

    public function renderValidateEmail(): string
    {
        $email = strtolower($this->getParameter('email'));
        $message = '';
        if (!$this->container->getConverter()->parseTextDocument($email))
        {
            $message = 'Zadejte validní FIM UHK email';
        }

        if (array_key_exists($email, array_change_key_case($this->container->getBlackListManager()
            ->getAll('email', ['state' => \Classes\BlackList\Manager::NOT_BLACKLISTED]))))
        {
            $message = 'Přístup do rezervačního systému Vám byl zakázán';
        }
        
        return $message;
    }

    public function renderCanRegisterThisEmail(): string
    {
        $message = '';
        if ($this->container->getLoginManager()->isEmailAlreadyUsed(strtolower($this->getParameter('email'))))
        {
            $message = 'Tento email je již použit u jiné rezervace';
        }

        return $message;
    }

    public function renderCanRenewPassword(): string
    {
        $message = '';
        if (is_null($this->container->getUserManager()->getByEmail(strtolower($this->getParameter('email')))))
        {
            $message = 'Tento email není použit u žádné rezervace';
        }

        return $message;
    }

    public function renderTestEmail(): void
    {
       $this->container->getConverter()->test();
    }

    public function renderDashboardTable(): string
    {

        $from = SF::getFilter() && SF::getFilter()->getFrom() ? SF::getFilter()->getFrom() : null;
        $to = SF::getFilter() && SF::getFilter()->getTo() ? SF::getFilter()->getTo() : null;
        $email = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        return $this->container->getReservationFacade()->getReportData($this->getParameter('page'), $from, $to, $email, true);
    }

    public function renderDeleteFromBlackList(): string
    {
        $this->container->getBlackListManager()->deleteByPrimary($this->getParameter('id'));

        return SF::renderLatte('blacklist', [
                'blacklist' => $this->container->getBlackListManager()->getAll()
            ]
        );
    }
    public function renderSetFree(): string
    {
        $this->container->getBlackListManager()->setFree($this->getParameter('id'));

        return SF::renderLatte('blacklist', [
                'blacklist' => $this->container->getBlackListManager()->getAll('', [BManager::STATE => BManager::BLACKLISTED])
            ]
        );
    }

    public function renderCheckExistingUsername(): void
    {
        $this->sendJSON(['alreadyExist' => $this->container->getLoginManager()->alreadyExist($this->getParameter('username'))]);
    }

    public function renderDoesAlreadyHaveReservation(): void
    {
        $from = $this->getParameter('dateFrom');
        $to = $this->getParameter('dateTo');
        $email = array_key_exists('email', $this->getParameter()) ? $this->getParameter('email'): UserData::getEmail();
        $this->sendJSON(['alreadyHave' => $this->container->getReservationManager()->doesHaveReservation(new DateTimeUtil($from), new DateTimeUtil($to), $email)]);
    }

    public function renderRemoveMember(): string
    {
        $id = $this->getParameter('id');
        $this->container->getTeamManager()->deleteFromTeam($id);

        if (UserData::getUserID() == $id)
        {
            UserData::disbandTeam();
            SF::setHeader("/admin/team");
        }

        return $this->container->getTeamFacade()->getMembers(true);
    }

    public function renderAddMember(): void
    {
        foreach ($this->getParameter('members') as $memberID)
        {
            $this->container->getTeamManager()->addToTeam(UserData::getTeam()->getId(), $memberID);
        }

        $this->sendJSON([
                'table' => $this->container->getTeamFacade()->getMembers(true),
                'addMember' => $this->container->getTeamFacade()->getMembersAdd(
                    $this->container->getUserManager()->getAllUsers(true),
                    $this->container->getTeamFacade()->getMembers(), true)
            ]
        );
    }

    public function renderGetMemberList(): string
    {
        return $this->container->getTeamFacade()->getMembersAdd(
                    $this->container->getUserManager()->getAllUsers(true),
                    $this->container->getTeamFacade()->getMembers(), true);
    }

    public function renderChangeMaintenance(): void
    {
        SF::changeMaintenanceState();
    }

    public function renderGetTeamRegistrationForm(): void
    {
        $this->container->getReservationFacade()->getTeamReservationForm($this->getParameter('date'));
    }

    public function renderChangePCPasswordAll(): string
    {
        $this->container->getServerUsersManager()->updateAllPasswords($this->getParameter('password'));
        return SF::renderLatte('computers', [
                'serverUsers' => $this->container->getServerUsersManager()->getAllUsers()
            ]
        );
    }

    public function renderChangePCState(): string
    {
        $this->container->getComputerManager()->managerUpdate([
                'state' => $this->getParameter('newState')
            ], $this->getParameter('pc')
        );
        return SF::renderLatte('computers', [
                'serverUsers' => $this->container->getServerUsersManager()->getAllUsers()
            ]
        );
    }

    public function renderCronLoggerTable(): string
    {
        return $this->container->getCronLogFacade()->getData($this->getParameter('page'), true);
    }

    public function renderUserTable(): string
    {
        return $this->container->getUserFacade()->getUserData($this->getParameter('page'), true);
    }

    public function renderCheckReservationRule(): void
    {
        $this->sendJSON($this->container->getRulesFacade()->checkTimeRule(new DateTimeUtil($this->getParameter('dateFrom')), new DateTimeUtil($this->getParameter('dateTo'))));
    }

    public function renderRulesTable(): string
    {
        if (array_key_exists('id', $this->getParameter()))
        {
            $this->container->getRulesManager()->updateRule(['myCount'=>$this->getParameter('myCount')], $this->getParameter('id'));
        }
        return $this->container->getRulesFacade()->getUserRules($this->getParameter('page'), true);
    }

    public function renderLogoutPC(): void
    {
        $this->container->getReservationFacade()->logoutPC($this->getParameter('name'), $this->getParameter("pc"));
    }
}