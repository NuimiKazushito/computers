<?php

use Classes\StaticFunctions as SF;
use Classes\UserData;

class Home extends \Classes\DefaultClass
    {
        public function renderHome()
        {
            $this->addData('toAdmin', UserData::getUserID());
            $this->addData('allInformation', $this->container->getInformationManager()->getAll());
            $this->addData('allGames', $this->container->getGamesFacade()->getAllGames());
            $this->addData('images', [
                    0 => '/www/mainAssets/images/room1.jpg',
                    1 => '/www/mainAssets/images/room3.jpg'
                ]
            );

            $this->renderLayout( 'index');
        }

        public function renderLogin()
        {
            if(UserData::checkLoggedIn())
            {
                SF::setHeader('/admin');
            }
            $this->renderLayout( 'login');
        }

        public function renderRegister()
        {
            if(UserData::checkLoggedIn())
            {
                SF::setHeader('/admin');
            }
            $this->setActiveModal(self::MODAL_RESERVATION);

            $this->addData('allComputers', $this->container->getComputerManager()->getAllComputers());

            if ($_GET)
            {
                $this->addData('oldRegister', $this->getParameter());
            }

            $this->renderLayout('registration');
        }

        public function renderAskForReservation()
        {
            if ($_POST)
            {
                $this->container->getReservationAskManager()->managerInsert($this->getParameter());
                $this->container->getMail()->sendReservationAskMail($this->getParameter());
                $this->container->getMail()->sendReservationAskApplicantMail($this->getParameter());
                SF::addSuccessMessage('Váš požadavek byl zadán, prosím počkejte na jeho vyřízení');
            }

            $this->renderLayout('askForReservation');
        }

        public function renderMaintenance()
        {
            $this->renderLayout('index', self::LATTE_MAINTENANCE, false);
        }
    }
