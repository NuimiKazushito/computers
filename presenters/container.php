<?php
namespace Presenters;

use Classes\Super\Mailer;
use dibi;
use ReflectionClass;
use ReflectionMethod;
use RuntimeException;

class Container
{
    const MODULE = 'Module';
    private function getInstance()
    {
        $trace = debug_backtrace();
        $trace = $trace[$trace[1]['function'] == 'getManager' ? 2 : 1];
        $reflectionMethod = new ReflectionMethod($trace['class'], $trace['function']);

        static $instanceList = [];
        try {
            $reflectionClass = new ReflectionClass ($this);
            $reflectionFunction = $reflectionClass->getMethod($reflectionMethod->getName());

            $class = (string) $reflectionFunction->getReturnType();

            if (!class_exists($class))
            {
                throw new RuntimeException($class . ' = does not exists');
            }

            if (!array_key_exists($class, $instanceList))
            {
                $reflection = new ReflectionClass($class);
                $instanceList[$class] = $reflection->newInstanceArgs(func_get_args());
            }
        } catch (\ReflectionException $e) {
            bdump('error');
            bdump($e);
        }
        return $instanceList[$class];
    }

    public function getDibi()
    {
        return dibi::getConnection();
    }

    public function getComputerManager() : \Classes\Computer\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getLoginManager() : \Classes\Login\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getReservationManager() : \Classes\Reservation\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getReservationFacade() : \Classes\Reservation\Facade
    {
        return $this->getInstance($this->getReservationManager(), $this->getReservationAllManager(),
            $this->getReservationAskManager(), $this->getUserManager(), $this->getCurrentPcPasswordManager(), $this->getPaginator());
    }

    public function getUserManager() : \Classes\User\Manager
    {
        return $this->getInstance($this->getDibi());
    }
    public function getUserFacade() : \Classes\User\Facade
    {
        return $this->getInstance($this->getPaginator(), $this->getUserManager());
    }

    public function getReservationAllManager() : \Classes\ReservationAll\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getReservationAllFacade() : \Classes\ReservationAll\Facade
    {
        return $this->getInstance($this->getReservationAllManager(), $this->getReservationManager(), $this->getUserManager());
    }

    public function getMail(): Mailer
    {
        return new Mailer();
    }

    public function getInformationManager() :\Classes\Information\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getInformationFacade() :\Classes\Information\Facade
    {
        return $this->getInstance($this->getInformationManager());
    }

    public function getGamesManager() :\Classes\Games\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getGamesFacade() :\Classes\Games\Facade
    {
        return $this->getInstance($this->getGamesManager());
    }

    public function getReservationAskManager() : \Classes\ReservationAsk\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getReservationAskFacade() : \Classes\ReservationAsk\Facade
    {
        return $this->getInstance($this->getReservationAskManager(), $this->getReservationManager());
    }

    public function getCurrentPcPasswordManager(): \Classes\CurrentPcPassword\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getServerUsersManager(): \Classes\ServerUsers\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getUHKEmails(): \Classes\UHKEmails\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getConverter(): \Classes\Super\Converter
    {
        return $this->getInstance($this->getUHKEmails());
    }

    public function getPaginator(): \Classes\Super\Paginator
    {
        return $this->getInstance($this->getDibi());
    }

    public function getReservationReport(): \Classes\Reservation\Report
    {
        return $this->getInstance($this->getReservationManager());
    }

    public function getBlackListManager(): \Classes\BlackList\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getTeamManager(): \Classes\Team\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getTeamFacade(): \Classes\Team\Facade
    {
        return $this->getInstance($this->getTeamManager());
    }

    public function getCronLogManager() : \Classes\CronLog\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getCronLogFacade() : \Classes\CronLog\Facade
    {
        return $this->getInstance($this->getPaginator(), $this->getCronLogManager());
    }

    public function getRulesManager() : \Classes\Rules\Manager
    {
        return $this->getInstance($this->getDibi());
    }

    public function getRulesFacade() : \Classes\Rules\Facade
    {
        return $this->getInstance($this->getRulesManager(), $this->getPaginator());
    }
}