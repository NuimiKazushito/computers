<?php

use Classes\BlackList\Manager as BManager;
use Classes\DateTimeUtil;
use Classes\Login\Manager as LManager;
use Classes\ReservationAsk\Manager as RAskManager;
use Classes\Rules\Manager as RManager;
use Classes\StaticFunctions as SF;
use Classes\team\Manager as TManager;
use Classes\UserData;

class Admin extends \Classes\DefaultClass
{
    public function renderDefault(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_HOME);
        $this->setActiveModal(self::MODAL_ADMIN);

        $search = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;

        $computers = $this->container->getComputerManager()->getAllComputers(true);
        if (UserData::isUser())
        {
            array_pop($computers);
            $this->addData('searchString', '');
        }

        $this->addData('searchString', $search);

        $this->addData('allComputers', $computers);
        $this->addData('otReservation', $this->container->getReservationFacade()->getAllOneTimeReservations($search));

        if (in_array(UserData::getPermission(), [LManager::ADMIN, LManager::GOD]))
        {
            $this->renderLayout('index');
        } else {
            $this->renderLayout('userIndex');
        }
    }

    public function renderComputers(): void
    {
        $this->isLoggedIn();
        $this->setActiveMenu(self::M_COMPUTERS);
        $this->setActiveModal(self::MODAL_COMPUTERS);
        $this->isSelect2Used(true);

        if ($_POST)
        {
            $data = $this->getParameter();
            $id = $data['passwordID'];
            unset($data['passwordID']);
            if ($id)
            {
                $now = new DateTimeUtil();
                $now->modify("+ 1 hour");
                $computer = $this->container->getCurrentPcPasswordManager()->getAllByPrimary($id);
                $userServer = $this->container->getServerUsersManager()->findUserByComputerID($computer['pcID']);

                $this->container->getCurrentPcPasswordManager()->managerUpdate([
                        \Classes\CurrentPcPassword\Manager::CURRENT_PASSWORD => $data['password'],
                        \Classes\CurrentPcPassword\Manager::END_DT => $now
                    ], $id);

                $this->container->getReservationFacade()->changePasswordPS($userServer['userName'], $data['password']);
                SF::addSuccessMessage('Heslo bylo úspěšně změněno');
            } else {
                $this->container->getServerUsersManager()->managerInsert($data);
                SF::addSuccessMessage('Uživatel byl úspěšně přidán');
            }
        }

        $this->addData('computers', $this->container->getComputerManager()->getAllComputers());
        $this->addData('serverUsers', $this->container->getServerUsersManager()->getAllUsers());

        $this->setRefreshMessage(true);
        $this->renderLayout('computers');
    }

    public function renderInformation(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_INFORMATION);
        $this->setActiveModal(self::MODAL_INFORMATION);

        if ($_POST)
        {
            $data = $this->getParameter();
            if ($data['informationID'] == '')
            {
                unset($data['informationID']);
                $this->container->getInformationManager()->managerInsert($data);
                SF::addSuccessMessage('Informace byly úspěšně vyplněny');
            } else {
                $id = $data['informationID'];
                unset($data['informationID']);;
                $this->container->getInformationManager()->managerUpdate($data, $id);
                SF::addSuccessMessage('Informace byla úspěšně upravena');
            }
        }

        $this->addData('allInformation', $this->container->getInformationFacade()->getALlInformation());

        $this->renderLayout('information');
    }

    public function renderGames(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_GAMES);
        $this->setActiveModal(self::MODAL_GAMES);

        if ($_POST)
        {
            $data = $this->getParameter();
            if ($data['gameID'] == '')
            {
                unset($data['gameID']);
                $this->container->getGamesManager()->managerInsert($data);
                SF::addSuccessMessage('Hra byla úspěšně přidána');
            } else {
                $id = $data['gameID'];
                unset($data['gameID']);;
                $this->container->getGamesManager()->managerUpdate($data, $id);
                SF::addSuccessMessage('Hra byla úspěšně upravena');
            }
        }

        $this->addData('allGames', $this->container->getGamesFacade()->getALlInformation());
        $this->addData('clients', $this->container->getGamesManager()->getAllClients());
        $this->addData('select2', true);

        $this->renderLayout('games');
    }

    public function renderReservationAsk(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_RESERVATION_ASK);

        if ($_GET)
        {
            if ($this->getParameter('accept') === 'true')
            {
                $data = $this->container->getReservationFacade()->acceptReservation($this->getParameter('id'));
                $this->container->getMail()->sendAcceptReservation($data['email'], $data['whenFrom'], $data['whenTo']);
                SF::addSuccessMessage('Rezervace byla úspěšně potvrzena a byl odeslán klíč k počítačům');
            } else {
                $this->refuseReservation($this->getParameter('id'), $this->getParameter('reason'));
            }
        }

        $this->addData('allReservationAsk', $this->container->getReservationAskFacade()->getAllActive());

        $this->setRefreshMessage(true);
        $this->renderLayout('reservationAsk');
    }

    public function renderDashboard(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_DASHBOARD);

        $from = SF::getFilter() && SF::getFilter()->getFrom() ? SF::getFilter()->getFrom() : null;
        $to = SF::getFilter() && SF::getFilter()->getTo() ? SF::getFilter()->getTo() : null;
        $email = SF::getFilter() && SF::getFilter()->getSearchString() ? SF::getFilter()->getSearchString() : null;
        $page = 1;

        $reportData = $this->container->getReservationFacade()->getReportData($page, $from, $to, $email);

        $this->addData('reportData', $reportData['data']);
        $this->addData('pages', $reportData['pages']);
        $this->addData('page', $page);
        $this->addData('ajax', 'dashboardTable');
        $this->addData('filterFrom', $from?->getDateFormFormat());
        $this->addData('filterTo', $to?->getDateFormFormat());
        $this->addData('filterEmail', $email);

        $this->setRefreshMessage(true);
        $this->renderLayout('dashboard');
    }

    public function renderSetDashboardFilters(): void
    {
        SF::setFilter($this->getParameter('dateFrom'), $this->getParameter('dateTo'), $this->getParameter('email'));
        SF::setHeader('/admin/dashboard');
    }

    public function renderDeleteFiltersDashboard(): void
    {
        SF::emptyFilter();
        SF::setHeader('/admin/dashboard');
    }

    public function renderSetMainFilters(): void
    {
        SF::setFilter('', '', $this->getParameter('searchString'));
        SF::setHeader('/admin');
    }

    public function renderDeleteFilters(): void
    {
        SF::emptyFilter();
        SF::setHeader('/admin');
    }


    public function renderGenerateReport(): void
    {
        $this->container->getReservationReport()->generateAttendanceReport();
    }

    public function renderBlackList(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_BLACK_LIST);
        $this->setActiveModal(self::MODAL_BLACK_LIST);

        if ($_POST)
        {
            $this->container->getBlackListManager()->managerInsert($this->getParameter());
            $this->container->getMail()->sendBlackListInfo($this->getParameter('email'), $this->getParameter('reason'));
            SF::addSuccessMessage('Uživatel byl přidán na blacklist');
        }

        $this->addData('blacklist', $this->container->getBlackListManager()->getAll('', [BManager::STATE => BManager::BLACKLISTED]));
        $this->addData('notBlacklisted', $this->container->getBlackListManager()->getAll('', [BManager::STATE => BManager::NOT_BLACKLISTED]));

        $this->setRefreshMessage(true);
        $this->renderLayout('blackList');
    }

    public function renderTeam(): void
    {
        $this->isLoggedIn(false);
        $this->setActiveMenu(self::M_TEAM);
        $this->setActiveModal(self::MODAL_TEAM);
        $this->isSelect2Used(true);

        if($_POST)
        {
            $name = $this->getParameter('name');
            $teamMembers = $this->getParameter('members');
            $teamMembers[] = UserData::getUserID();

            $teamID = $this->container->getTeamManager()->managerInsert([
                TManager::NAME => $name,
                TManager::USER => UserData::getUserID()
            ], true);

            if ($teamID)
            {
                foreach ($teamMembers as $memberID)
                {
                    $this->container->getTeamManager()->addToTeam($teamID, $memberID);
                }

                UserData::setTeam($name, $teamID, true);
                SF::addSuccessMessage('Tým byl vytvořen a členové byli přidáni');
            } else {
                SF::addErrorMessage('Použitá hodnota je již použita jinde');
            }
        }

        $users = $this->container->getUserManager()->getAllUsers(true);
        $members = $this->container->getTeamFacade()->getMembers();
        $usersAdd = $this->container->getTeamFacade()->getMembersAdd($users, $members);

        $this->addData('users', $users);
        $this->addData('usersAdd', $usersAdd);
        $this->addData('members', $members);

        $this->renderLayout('team');
    }

    public function renderTestEmail(): void
    {
        $this->addData('reason', 'test');
        $this->renderLayout('email/blackList');
    }

    public function renderShutDownMaintenance(): void
    {
        SF::changeMaintenanceState();
    }

    public function renderCronLog()
    {
        $this->setActiveMenu(self::M_CRON);

        $page = 1;
        $cronData = $this->container->getCronLogFacade()->getData($page);
        $this->addData('pages', $cronData['pages']);
        $this->addData('cronLog', $cronData['data']);
        $this->addData('page', $page);
        $this->addData('ajax', 'cronLoggerTable');

        $this->renderLayout('cronLog');
    }

    public function renderRules()
    {
        $this->setActiveMenu(self::M_RULES);
        $this->setActiveModal(self::MODAL_RULE);
        $this->isSelect2Used(true);

        if ($_POST)
        {
            if (!array_key_exists('id', $this->getParameter()))
            {
                $insert[RManager::PC] = $this->getParameter('numberOfPCs')?: null;
                $insert[RManager::HOURS] = $this->getParameter('hour')?: null;
                $insert[RManager::PER] = $this->getParameter('per')?: null;
                $insert[RManager::DESCRIPTION] = $this->getParameter('description');
                $this->container->getRulesManager()->managerInsert($insert);
                SF::addSuccessMessage("Pravidlo bylo úspěšně vytvořeno");
            }
        }

        $this->addData('rules', $this->container->getRulesManager()->getAll());


        $page = 1;
        $rulesData = $this->container->getRulesFacade()->getUserRules($page);

        $this->addData('userRules', $rulesData['data']);
        $this->addData('pages', $rulesData['pages']);
        $this->addData('page', $page);
        $this->addData('ajax', 'rulesTable');

        $this->setRefreshMessage(true);

        $this->renderLayout('rules');
    }

    public function renderCalendar()
    {
        $this->setActiveMenu(self::M_CALENDAR);

        $this->addData('events', json_encode($this->container->getReservationFacade()->getAllReservationsToCalendar(11)));

        $this->renderLayout('calendar');
    }


    private function refuseReservation(int $reservationID, string $reason): void
    {
        $data = $this->container->getReservationAskManager()->getAllByPrimary($reservationID);

        $this->container->getReservationAskManager()->setSolved($reservationID);

        $whenFrom = new DateTimeUtil($data[RAskManager::DATE_WHEN]);
        $whenFrom->setTime($data[RAskManager::TIME_FROM]->h, $data[RAskManager::TIME_FROM]->i);
        $whenTo = new DateTimeUtil($data[RAskManager::DATE_WHEN]);
        $whenTo->setTime($data[RAskManager::TIME_TO]->h, $data[RAskManager::TIME_TO]->i);
        $this->container->getMail()->sendRefuseReservation($data[RAskManager::EMAIL], $whenFrom, $whenTo, $reason);
        SF::addSuccessMessage('Rezervace byla úspěšně zamítnuta a bylo odesláno obeznámení se situací rezervace');
    }
}
